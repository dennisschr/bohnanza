package nl.utwente.bestpractice.gui.javafx.view;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import nl.utwente.bestpractice.gui.javafx.ResourceLoader;
import nl.utwente.bestpractice.model.Treasury;

public class TreasuryPane extends GridPane{
    
    private Treasury treasury;

    public TreasuryPane(Treasury treasury){
	this.treasury = treasury;
	
	if(treasury==null || treasury.getCoins() == 0){
	    // Empty treasury
	    this.add(new Label("0 coin(s)"), 0,0);
	    this.add(new ImageView(ResourceLoader.loadImage("cards/Empty.png")), 0, 1);
	}else{
	    this.add(new Label(treasury.getCoins() + " coin(s)"), 0,0);
	    this.add(new ImageView(ResourceLoader.loadImage("cards/Coin.png")), 0, 1);
	}
	
	this.getStyleClass().add("treasury-pane");
    }
}
