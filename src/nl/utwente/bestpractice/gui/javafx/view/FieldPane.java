package nl.utwente.bestpractice.gui.javafx.view;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import nl.utwente.bestpractice.gui.javafx.ResourceLoader;
import nl.utwente.bestpractice.model.Field;

public class FieldPane extends GridPane{

    private Field field;
    private CheckBox checkBox;
    
    public FieldPane(Field field){
	this(field, true);
    }

    public FieldPane(Field field, boolean selectable) {
	this.field = field;
	
	if(field==null){
	    // No field
	    this.add(new Label("No Field"), 0,0);
		this.add(new ImageView(ResourceLoader.loadImage("cards/Bean Field.png")), 0, 1);
	}else{
	    if(field.getBeanType() == null){
		// Empty field
		this.add(new Label("Empty"), 0,0);
		this.add(new ImageView(ResourceLoader.loadCardImage(null)), 0, 1);
		if(selectable){
		    checkBox = new CheckBox();
		    this.add(checkBox, 0, 2);
		}
	    }else{
		this.add(new Label(field.getBeanCount() + " bean(s)"), 0,0);
		this.add(new CardPane(field.getCards().get(0), false), 0, 1);
		if(selectable){
		    checkBox = new CheckBox();
		    this.add(checkBox, 0, 2);
		}
	    }
	}
	
	
	
	this.getStyleClass().add("field-pane");
    }

    public FieldPane() {
	this(null);
    }

    public boolean isSelected() {
	return checkBox != null && checkBox.isSelected();
    }

    public Field getField() {
	return this.field;
    }
}
