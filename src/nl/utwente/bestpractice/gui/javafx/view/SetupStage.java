package nl.utwente.bestpractice.gui.javafx.view;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import nl.utwente.bestpractice.exception.GameAlreadyStartedException;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.GameDescription;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.gui.javafx.BohnanzaApplication;
import nl.utwente.bestpractice.gui.javafx.JavaFXPlayerStrategy;

public class SetupStage extends Stage implements EventHandler<ActionEvent> {
    private ChoiceBox<GameDescription> type;
    private ChoiceBox<Integer> numberOfPlayers;
    private Button start;
    private BohnanzaApplication application;

    public SetupStage(BohnanzaApplication application) {
	super();
	this.application = application;
	this.build();
    }

    protected void build() {
	this.setTitle("Bohnanza");

	// Build grid
	GridPane grid = new GridPane();
	grid.setAlignment(Pos.CENTER);
	grid.setHgap(10);
	grid.setVgap(10);

	// Welcome
	Text scenetitle = new Text("Bohnanza");
	scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
	grid.add(scenetitle, 0, 0, 2, 1);

	// Select type
	Label typeLabel = new Label("Bohnanza type");

	this.type = new ChoiceBox<GameDescription>(
		FXCollections.observableList(this.application.getGameTypes()));
	type.getSelectionModel().select(0);

	// Number of players
	Label numberOfPlayersLabel = new Label("Number of players");
	this.numberOfPlayers = new ChoiceBox<Integer>(
		FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7));
	numberOfPlayers.getSelectionModel().select(2);

	// Start button
	this.start = new Button("Start");
	start.setOnAction(this);

	// Add controls to grid
	grid.add(typeLabel, 0, 1);
	grid.add(type, 1, 1);
	grid.add(numberOfPlayersLabel, 0, 2);
	grid.add(numberOfPlayers, 1, 2);
	grid.add(start, 1, 3);

	Scene scene = new Scene(grid, 400, 250);
	this.setScene(scene);
    }

    @Override
    public void handle(ActionEvent event) {
	if (event.getSource().equals(this.start)) {
	    // Get selection
	    GameDescription description = (GameDescription) this.type
		    .getSelectionModel().selectedItemProperty().get();
	    Integer number = (Integer) this.numberOfPlayers.getSelectionModel()
		    .selectedItemProperty().get();

	    // TODO add checks

	    // Create game
	    Game game = new Game(description);

	    // Add players
	    try {
		for (int i = 0; i < number; i++) {
		    Player player = description.createPlayer("Player-" + i);
		    player.setStrategy(new JavaFXPlayerStrategy());
		    game.addPlayer(player);
		}
	    } catch (GameAlreadyStartedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    // Start game
	    this.application.startGame(game);
	    this.close();
	}

    }
}
