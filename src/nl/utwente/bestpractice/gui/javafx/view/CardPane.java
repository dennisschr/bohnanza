package nl.utwente.bestpractice.gui.javafx.view;

import java.util.Map.Entry;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import nl.utwente.bestpractice.gui.javafx.ResourceLoader;
import nl.utwente.bestpractice.model.Card;

public class CardPane extends GridPane {
    
    private ImageView imageView;
    private CheckBox checkBox;
    private Card card;

    public CardPane(Card card){
	this(card, true);
    }

    public CardPane(Card card, boolean selectable) {
	super();
	this.card = card;
	
	this.imageView = new ImageView();
	this.imageView.setImage(ResourceLoader.loadCardImage(card.getBeanType()));
	
	this.add(new Label(card.getBeanType().getAmount() + " " + card.getBeanType().getName()), 0, 0);
	this.add(this.imageView, 0, 1);
	String meter = "";
	for(Entry<Integer, Integer> entry : card.getBeanType().getBeanometer().entrySet()){
	    meter += entry.getValue() + ":" + entry.getKey() + " "; 
	}
	
	
	this.add(new Label(meter), 0, 2);
	
	if(selectable){
	    checkBox = new CheckBox();
	    this.add(checkBox, 0, 3);
	}
	
	this.getStyleClass().add("card-pane");
    }

    public boolean isSelected() {
	return checkBox != null && checkBox.isSelected();
    }

    public Card getCard() {
	return this.card;
    }

}
