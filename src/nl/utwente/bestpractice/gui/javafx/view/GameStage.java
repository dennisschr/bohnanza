package nl.utwente.bestpractice.gui.javafx.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Field;

public class GameStage extends Stage {

    private HBox playerFieldsPane;
    private Map<String, HBox> opponentsFieldsPanes;
    private VBox playerPane;
    private HBox playerHandPane;
    private BorderPane root;
    private GameController controller;
    private VBox actionsPane;
    private VBox gameInfoPane;
    private BorderPane gamePane;
    private BorderPane fieldsPane;

    public GameStage(GameController controller) {
	super();
	this.controller = controller;
	this.build();
    }

    protected void build() {
	this.setTitle("Bohnanza - " + controller.getPlayer().getUsername());

	root = new BorderPane();
	// borderPane.setLeft(buildPlayerCardsPane());
	root.setTop(buildOtherPlayerPane());
	root.setLeft(buildFieldsPane());
	root.setRight(buildGameDetailsPane());
	root.setBottom(buildPlayerPane());
	root.setCenter(buildGamePane());

	final ListView<String> log = new ListView<String>(
		this.controller.getMessages());
	log.setMaxHeight(100);
	// Auto scroll
	// Source:
	// http://stackoverflow.com/questions/14779135/javafx-tableview-auto-scroll-to-the-last
	log.getItems().addListener(new ListChangeListener<String>() {
	    @Override
	    public void onChanged(Change<? extends String> c) {
		c.next();
		int size = log.getItems().size();
		if (size > 0) {
		    log.scrollTo(size - 1);
		}
	    }
	});
	root.setTop(log);

	Scene scene = new Scene(root, 1000, 800);
	scene.getStylesheets().add("/styles/game.css");

	this.setScene(scene);
    }

    private Node buildFieldsPane() {
	fieldsPane = new BorderPane();
	
	playerFieldsPane = new HBox();
	
	opponentsFieldsPanes = new HashMap<String, HBox>();
	VBox opponents = new VBox();
	
	// Create a HBox for every opponnent
	for(Player player : this.controller.getApplication().getGame().getPlayers()){
	    if(!player.equals(this.controller.getPlayer())){
		HBox box = new HBox();
		opponentsFieldsPanes.put(player.getUsername(), box);
		opponents.getChildren().add(new TitledPane(player.getUsername(), box));
	    }
	}
	
	fieldsPane.setCenter(new ScrollPane(opponents));
	fieldsPane.setBottom(new TitledPane("You", playerFieldsPane));
	
	for(Player player : this.controller.getApplication().getGame().getPlayers()){
	    updatePlayerFields(player);
	}
	
	return fieldsPane;
    }

    private Node buildGamePane() {
	gamePane = new BorderPane();
	
	updateGamePane();

	return gamePane;
    }

    private Node buildOtherPlayerPane() {
	FlowPane otherPlayerPane = new FlowPane();

	return otherPlayerPane;
    }

    private Node buildPlayerPane() {
	playerPane = new VBox();

	playerHandPane = new HBox();

	playerPane.getChildren().add(new TitledPane("Hand", playerHandPane));

	updatePlayerHand();

	return playerPane;
    }

    private Node buildGameDetailsPane() {
	VBox gameDetailsPane = new VBox();
	gameDetailsPane.setMaxWidth(200);

	gameInfoPane = new VBox();
	actionsPane = new VBox();

	updateGameInfo();

	gameDetailsPane.getChildren().add(new TitledPane("Info", gameInfoPane));
	gameDetailsPane.getChildren().add(
		new TitledPane("Actions", actionsPane));

	updateActions();

	return gameDetailsPane;
    }

    public void updateGameInfo() {
	if (gameInfoPane == null)
	    return;
	
	gameInfoPane.getChildren().clear();

	Label shuffles = new Label("Shuffles: "
		+ this.controller.getApplication().getGame().getShuffles());
	shuffles.setWrapText(true);
	/*
	 * shuffles.textProperty()
	 * .bind(this.controller.getShuffleProperty().asString(
	 * "Shuffles: %d"));
	 */
	Label deckSize = new Label("Deck: "
		+ this.controller.getApplication().getGame().getDeck().size());
	deckSize.setWrapText(true);
	/*
	 * deckSize.textProperty().bind(
	 * this.controller.getDeckSizeProperty().asString("Deck: %d"));
	 */

	Label currentPlayer = new Label("Current Player: "
		+ this.controller.getApplication().getGame().getCurrentPlayer()
			.getUsername());
	currentPlayer.setWrapText(true);

	Label discardPileTopCardLabel = new Label("Discard pile top card:");
	DiscardPilePane discardPileTopCard = new DiscardPilePane(
		this.controller.getApplication().getGame().getDiscardPile());
	discardPileTopCardLabel.setWrapText(true);

	Label currentPhase = new Label("Current Phase: "
		+ this.controller.getApplication().getGame().getCurrentPhase()
			.getName());
	currentPhase.setWrapText(true);
	Label currentPhaseDescription = new Label("Description: "
		+ this.controller.getApplication().getGame().getCurrentPhase()
			.getDescription());
	currentPhaseDescription.setWrapText(true);

	gameInfoPane.getChildren().add(shuffles);
	gameInfoPane.getChildren().add(deckSize);
	gameInfoPane.getChildren().add(currentPlayer);
	gameInfoPane.getChildren().add(currentPhase);
	gameInfoPane.getChildren().add(currentPhaseDescription);
	gameInfoPane.getChildren().add(discardPileTopCardLabel);
	gameInfoPane.getChildren().add(discardPileTopCard);
    }

    public void updatePlayerFields(Player player) {
	if (playerFieldsPane == null || opponentsFieldsPanes == null)
	    return;
	
	
	HBox pane;
	boolean selectable;
	if(player.equals(this.controller.getPlayer())){
	    // Own player
	    pane = playerFieldsPane;
	    selectable = true;
	}else{
	    // Opponnent
	    pane = opponentsFieldsPanes.get(player.getUsername());
	    selectable = false;
	}

	pane.getChildren().clear();

	// Add fields
	for (int i = 0; i < this.controller.getApplication().getGame()
		.getDescription().getMaximumFields(); i++) {
	    // Create pane
	    FieldPane fieldPane;
	    if (i < player.getFields().size()) {
		fieldPane = new FieldPane(player
			.getFields().get(i), selectable);
	    } else {
		fieldPane = new FieldPane(null, selectable);
	    }
	    pane.getChildren().add(fieldPane);
	}

	// Add coins
	pane.getChildren().add(
		new TreasuryPane(player.getTreasury()));
    }

    public void updatePlayerHand() {
	if (playerHandPane == null)
	    return;

	playerHandPane.getChildren().clear();

	for (Card card : this.controller.getPlayer().getHand().getCards()) {
	    playerHandPane.getChildren().add(new CardPane(card));
	}
    }

    public void updateActions() {
	if (actionsPane == null)
	    return;

	actionsPane.getChildren().clear();
	List<Class<? extends Action>> possibleActions = this.controller
		.getApplication().getGame().getCurrentPhase()
		.getPossibleActions(this.controller.getPlayer());

	for (Class<? extends Action> action : possibleActions) {
	    // Create button
	    Button button = new Button(this.controller.getApplication()
		    .getGame().getLocalization()
		    .getString("ACTION_" + action.getName().toUpperCase()));
	    // Set command
	    button.setOnAction(this.controller.getActionCommandFactory().get(
		    action));
	    // Add to pane
	    actionsPane.getChildren().add(button);
	}
    }

    public void updateGamePane() {
        // TODO Auto-generated method stub
        
    }

    public List<Field> getSelectedFields() {
	List<Field> result = new ArrayList<Field>();
	
	// Own fields
	for (Node node : this.playerFieldsPane.getChildren()) {
	    if (node instanceof FieldPane) {
		FieldPane f = (FieldPane) node;
		if (f.isSelected()) {
		    result.add(f.getField());
		}
	    }
	}
	
	// Opponent fields
	for (Node box : this.opponentsFieldsPanes.values()) {
	    if(box instanceof Pane){
		Pane pane = (Pane)box;
		for (Node node : pane.getChildren()) {
		    if (node instanceof FieldPane) {
			FieldPane f = (FieldPane) node;
			if (f.isSelected()) {
			    result.add(f.getField());
			}
		    }
		}
	    }
	}
	
	return result;
    }

    public List<Card> getSelectedCards() {
	List<Card> result = new ArrayList<Card>();
	for (Node node : this.playerHandPane.getChildren()) {
	    if (node instanceof CardPane) {
		CardPane c = (CardPane) node;
		if (c.isSelected()) {
		    result.add(c.getCard());
		}
	    }
	}
	return result;
    }
}
