package nl.utwente.bestpractice.gui.javafx.view;

import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import nl.utwente.bestpractice.gui.javafx.ResourceLoader;
import nl.utwente.bestpractice.model.DiscardPile;

public class DiscardPilePane extends GridPane {

    public DiscardPilePane(DiscardPile pile) {
	if (pile == null || pile.getTopCard() == null) {
	    // No cards
	    this.add(new ImageView(ResourceLoader.loadCardImage(null)), 0, 0);
	} else {
	    this.add(new CardPane(pile.getTopCard()), 0, 0);
	}

    }
}
