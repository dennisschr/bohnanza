package nl.utwente.bestpractice.gui.javafx;

import nl.utwente.bestpractice.action.Action;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ActionExecutor implements EventHandler<ActionEvent> {

    private Class<? extends Action> actionClass;

    public ActionExecutor(Class<? extends Action> actionClass) {
	this.actionClass = actionClass;
    }

    @Override
    public void handle(ActionEvent event) {
	System.out.println(this.actionClass.getName());

    }

}
