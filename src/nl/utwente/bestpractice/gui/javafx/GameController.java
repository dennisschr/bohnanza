package nl.utwente.bestpractice.gui.javafx;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import nl.utwente.bestpractice.action.BeanPlantAction;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.action.PhaseStopAction;
import nl.utwente.bestpractice.action.TradeAcceptAction;
import nl.utwente.bestpractice.action.TradeRequestAction;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.gui.javafx.action.ActionCommandFactory;
import nl.utwente.bestpractice.gui.javafx.action.BeanPlantActionCommand;
import nl.utwente.bestpractice.gui.javafx.action.BuyFieldActionCommand;
import nl.utwente.bestpractice.gui.javafx.action.FieldHarvestActionCommand;
import nl.utwente.bestpractice.gui.javafx.action.PhaseStopActionCommand;
import nl.utwente.bestpractice.gui.javafx.action.TradeAcceptActionCommand;
import nl.utwente.bestpractice.gui.javafx.action.TradeRequestActionCommand;
import nl.utwente.bestpractice.gui.javafx.view.GameStage;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Field;

public class GameController implements EventHandler<ActionEvent>, Observer {

    private Player player;
    private BohnanzaApplication application;
    private GameStage view;
    private ActionCommandFactory actionCommandFactory;
    private ObservableList<String> messages;

    // private IntegerProperty shuffleProperty;
    // private IntegerProperty deckSizeProperty;
    // private ObjectProperty<Card> discardPileTopCardProperty;

    public GameController(BohnanzaApplication application, Player player) {
	this.application = application;
	this.player = player;
	
	// Observe players and game
	for(Player p : this.application.getGame().getPlayers()){
	    p.addObserver(this);
	};
	this.application.getGame().addObserver(this);
	
	// Log messages
	this.messages = FXCollections.observableArrayList();
	
	/*
	 * this.shuffleProperty = new SimpleIntegerProperty();
	 * this.shuffleProperty.set(application.getGame().getShuffles());
	 * this.deckSizeProperty = new SimpleIntegerProperty();
	 * this.deckSizeProperty.set(application.getGame().getDeck().size());
	 * 
	 * this.discardPileTopCardProperty = new SimpleObjectProperty<Card>();
	 * this.discardPileTopCardProperty.set(application.getGame()
	 * .getDiscardPile().getTopCard());
	 */

	// Create the ActionCommandFactory and fill it
	this.actionCommandFactory = new ActionCommandFactory();
	this.actionCommandFactory.addActionCommand(BeanPlantAction.class, new BeanPlantActionCommand(this));
	this.actionCommandFactory.addActionCommand(FieldBuyAction.class, new BuyFieldActionCommand(this));
	this.actionCommandFactory.addActionCommand(FieldHarvestAction.class, new FieldHarvestActionCommand(this));
	this.actionCommandFactory.addActionCommand(PhaseStopAction.class, new PhaseStopActionCommand(this));
	this.actionCommandFactory.addActionCommand(TradeRequestAction.class, new TradeRequestActionCommand(this));
	this.actionCommandFactory.addActionCommand(TradeAcceptAction.class, new TradeAcceptActionCommand(this));

	this.view = new GameStage(this);
	this.view.show();
    }

    @Override
    public void update(Observable observable, Object data) {
	if (observable instanceof Player) {
	    Player.ObservableChange change = (Player.ObservableChange) data;
	    Player player = (Player) observable;
	    switch (change) {
	    case HAND:
		if(player.equals(this.getPlayer())) this.view.updatePlayerHand();
		break;
	    case FIELDS:
	    case TREASURY:
		this.view.updatePlayerFields(player);
		break;
	    case TRADING:
		this.view.updateGamePane();
		break;
	    }
	} else if (observable instanceof Game) {
	    Game.ObservableChange change = (Game.ObservableChange) data;
	    switch (change) {
	    case PHASE:
	    case PLAYER_TURN:
		this.view.updateActions();
		break;
	    }

	    this.view.updateGameInfo();
	}

    }

    @Override
    public void handle(ActionEvent arg0) {
	// TODO Auto-generated method stub

    }

    public BohnanzaApplication getApplication() {
	return application;
    }

    public Player getPlayer() {
	return player;
    }

    public ActionCommandFactory getActionCommandFactory() {
	return actionCommandFactory;
    }

    /**
     * Get the field selected by the player. If multiple fields are selected,
     * null is returned
     * 
     * @return
     */
    public Field getSelectedField() {
	List<Field> selected = this.getSelectedFields();
	return selected.size() == 1 ? selected.get(0) : null;
    }

    public List<Field> getSelectedFields() {
	return this.view.getSelectedFields();
    }

    /**
     * Get the card selected by the player. If multple cards are selected, null
     * is returned
     * 
     * @return
     */
    public Card getSelectedCard() {
	List<Card> selected = this.getSelectedCards();
	return selected.size() == 1 ? selected.get(0) : null;
    }

    public List<Card> getSelectedCards() {
	return this.view.getSelectedCards();
    }

    public ObservableList<String> getMessages() {
	return this.messages;
    }

    /*
     * public IntegerProperty getShuffleProperty() { return shuffleProperty; }
     * 
     * public IntegerProperty getDeckSizeProperty() { return deckSizeProperty; }
     * 
     * public ObjectProperty<Card> getDiscardPileTopCardProperty() { return
     * discardPileTopCardProperty; }
     */

}
