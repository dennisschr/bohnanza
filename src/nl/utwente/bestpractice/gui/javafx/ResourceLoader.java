package nl.utwente.bestpractice.gui.javafx;

import java.io.InputStream;

import javafx.scene.image.Image;
import nl.utwente.bestpractice.model.BeanType;

public class ResourceLoader {
    public static Image loadImage(String fileName){
	//System.out.println("/images/" + fileName);
	//System.out.println(ResourceLoader.class.getResource("/images/" + fileName));
       
	InputStream stream = ResourceLoader.class.getResourceAsStream("/images/" + fileName);
	
	return stream != null ? new Image(stream) : null;
    }

    public static Image loadCardImage(BeanType beanType) {
	if(beanType == null){
	    return loadImage("cards/Empty.png");
	}else{
	    return loadImage("cards/" + beanType.getName() + " Bean.png");
	}
	
    }
}
