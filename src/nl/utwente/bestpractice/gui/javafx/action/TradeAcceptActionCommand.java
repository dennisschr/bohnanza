package nl.utwente.bestpractice.gui.javafx.action;

import javafx.event.ActionEvent;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.localization.MainValues;

public class TradeAcceptActionCommand extends ActionCommand {

    public TradeAcceptActionCommand(GameController controller) {
	super(controller);
    }

    @Override
    public void execute(ActionEvent event) throws ActionNotAllowedException {
	throw new ActionNotAllowedException("Trading not yet possible");
    }
}
