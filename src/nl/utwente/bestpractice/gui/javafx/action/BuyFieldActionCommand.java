package nl.utwente.bestpractice.gui.javafx.action;

import javafx.event.ActionEvent;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.localization.MainValues;

public class BuyFieldActionCommand extends ActionCommand {

    public BuyFieldActionCommand(GameController controller) {
	super(controller);
    }

    @Override
    public void execute(ActionEvent event) throws ActionNotAllowedException {
	FieldBuyAction action = new FieldBuyAction(this.controller.getPlayer());

	this.controller.getApplication().getGame().submitAction(action);
    }
}
