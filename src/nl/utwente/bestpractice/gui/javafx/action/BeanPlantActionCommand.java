package nl.utwente.bestpractice.gui.javafx.action;

import javafx.event.ActionEvent;
import nl.utwente.bestpractice.action.BeanPlantAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.localization.MainValues;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Field;

public class BeanPlantActionCommand extends ActionCommand {

    public BeanPlantActionCommand(GameController controller) {
	super(controller);
    }

    @Override
    public void execute(ActionEvent event) throws ActionNotAllowedException {
	Field field = this.controller.getSelectedField();
	Card card = this.controller.getSelectedCard();

	if (field == null || card == null) {
	    if (field == null) {
		this.controller.getMessages().add(
			this.controller.getApplication().getGame()
				.getLocalization()
				.getString(MainValues.SELECT_ONE_FIELD));
	    }
	    if (card == null) {
		this.controller.getMessages().add(
			this.controller.getApplication().getGame()
				.getLocalization()
				.getString(MainValues.SELECT_ONE_CARD));
	    }
	    return;
	}

	BeanPlantAction action = new BeanPlantAction(
		this.controller.getPlayer(), field, card);

	this.controller.getApplication().getGame().submitAction(action);
    }
}
