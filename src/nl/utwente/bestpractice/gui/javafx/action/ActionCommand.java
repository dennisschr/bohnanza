package nl.utwente.bestpractice.gui.javafx.action;

import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.localization.MainValues;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * In order to create a link between a button press and the submitting of an
 * action (for example plant a bean), an ActionCommand is used which, using the
 * interface, picks the correct parameters (for example selected cards) and
 * submits them with the corresponding action. As this is interface dependent
 * and it is bad practice to put it in the Action classes, we use this executor
 * much like the Command pattern.
 */
public class ActionCommand implements EventHandler<ActionEvent> {

    protected GameController controller;

    public ActionCommand(GameController controller) {
	this.controller = controller;
    }

    @Override
    public void handle(ActionEvent event) {
	try {
	    this.execute(event);
	} catch (ActionNotAllowedException e) {
	    this.controller.getMessages().add(
		    this.controller.getApplication().getGame()
			    .getLocalization()
			    .getString(MainValues.ACTION_NOT_ALLOWED)
			    + " " + e.getMessage());
	}
    }

    public void execute(ActionEvent event) throws ActionNotAllowedException {

    }

}
