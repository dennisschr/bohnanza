package nl.utwente.bestpractice.gui.javafx.action;

import java.util.HashMap;
import java.util.Map;

import nl.utwente.bestpractice.action.Action;

public class ActionCommandFactory {

    private Map<Class<? extends Action>, ActionCommand> actionCommands;
    
    public ActionCommandFactory(){
	this.actionCommands = new HashMap<Class<? extends Action>, ActionCommand>();
    }

    public void addActionCommand(Class<? extends Action> action, ActionCommand command){
	this.actionCommands.put(action, command);
    }
    
    public ActionCommand get(Class<? extends Action> action){
	return this.actionCommands.get(action);
    }
}
