package nl.utwente.bestpractice.gui.javafx.action;

import javafx.event.ActionEvent;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;

public class TradeRequestActionCommand extends ActionCommand {

    public TradeRequestActionCommand(GameController controller) {
	super(controller);
    }

    @Override
    public void execute(ActionEvent event) throws ActionNotAllowedException {
	throw new ActionNotAllowedException("Trading not yet possible");
    }
}
