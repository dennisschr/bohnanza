package nl.utwente.bestpractice.gui.javafx.action;

import javafx.event.ActionEvent;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.localization.MainValues;
import nl.utwente.bestpractice.model.Field;

public class FieldHarvestActionCommand extends ActionCommand {

    public FieldHarvestActionCommand(GameController controller) {
	super(controller);
    }

    @Override
    public void execute(ActionEvent event) throws ActionNotAllowedException {
	Field field = this.controller.getSelectedField();

	if (field == null) {
	    this.controller.getMessages().add(
		    this.controller.getApplication().getGame()
			    .getLocalization()
			    .getString(MainValues.SELECT_ONE_FIELD));
	    return;
	}

	FieldHarvestAction action = new FieldHarvestAction(
		this.controller.getPlayer(), field);

	this.controller.getApplication().getGame().submitAction(action);
    }
}
