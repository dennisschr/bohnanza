package nl.utwente.bestpractice.gui.javafx.action;

import javafx.event.ActionEvent;
import nl.utwente.bestpractice.action.PhaseStopAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.gui.javafx.GameController;
import nl.utwente.bestpractice.localization.MainValues;

public class PhaseStopActionCommand extends ActionCommand {

    public PhaseStopActionCommand(GameController controller) {
	super(controller);
    }

    @Override
    public void execute(ActionEvent event) throws ActionNotAllowedException {
	PhaseStopAction action = new PhaseStopAction(
		this.controller.getPlayer());

	this.controller.getApplication().getGame().submitAction(action);
    }
}
