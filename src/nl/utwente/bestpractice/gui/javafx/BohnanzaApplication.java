package nl.utwente.bestpractice.gui.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import nl.utwente.bestpractice.exception.InvalidPlayerCountException;
import nl.utwente.bestpractice.game.AlCabohneForTwoGameDescription;
import nl.utwente.bestpractice.game.DefaultGameDescription;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.GameDescription;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.gui.javafx.view.SetupStage;

public class BohnanzaApplication extends Application {

    public List<GameDescription> gameTypes;
    private Game game;

    @Override
    public void start(Stage primaryStage) throws Exception {
	gameTypes = new ArrayList<GameDescription>(2);
	gameTypes.add(new DefaultGameDescription());
	gameTypes.add(new AlCabohneForTwoGameDescription());

	/*
	 * primaryStage.setTitle("Hello World!"); Button btn = new Button();
	 * btn.setText("Say 'Hello World'"); btn.setOnAction(new
	 * EventHandler<ActionEvent>() {
	 * 
	 * @Override public void handle(ActionEvent event) {
	 * System.out.println("Hello World!"); } });
	 * 
	 * StackPane root = new StackPane(); root.getChildren().add(btn);
	 * primaryStage.setScene(new Scene(root, 300, 250));
	 * primaryStage.show();
	 */
	buildSetupStage();
    }

    public void buildSetupStage() {
	Stage stage = new SetupStage(this);
	stage.show();
    }

    public List<GameDescription> getGameTypes() {
	return gameTypes;
    }

    public static void main(String[] args) {
	launch(args);
    }

    /**
     * Start the given game by building a GUI for it and start the game
     * 
     * @param game
     *            The game to start
     */
    public void startGame(Game game) {
	this.game = game;

	for (Player player : game.getPlayers()) {
	    this.buildGameStage(player);
	}
	
	try {
	    game.start();
	} catch (InvalidPlayerCountException e) {
	    e.printStackTrace();
	}

    }

    /**
     * Build a stage (window) for the given game for the given player
     * 
     * @param game
     * @param player
     */
    private void buildGameStage(Player player) {
	if (player.getStrategy() instanceof JavaFXPlayerStrategy) {
	   GameController controller = new GameController(this, player);
	    ((JavaFXPlayerStrategy)player.getStrategy()).setController(controller);
	}

    }

    public Game getGame() {
	return game;
    }

}
