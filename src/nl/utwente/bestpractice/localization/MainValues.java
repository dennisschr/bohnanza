package nl.utwente.bestpractice.localization;

public enum MainValues {
    /** Indicates that the type of the field and the bean were not the same */
    FIELD_PLANT_INVALID_TYPE, 
    /** The maximum number of plant actions has been reached in this phase */
    FIELD_PLANT_PHASE_MAXIMUM_REACHED, 
    /** You need to plant more beans before this phase can end */
    FIELD_PLANT_PHASE_MINIMUM_NEEDED, 
    ACTION_NOT_ALLOWED, SELECT_ONE_FIELD, SELECT_ONE_CARD, HAND_ONLY_FIRST, FIELD_BUY_NOT_ENOUGH_COINS, FIELD_BUY_MAXIMUM_FIELDS_REACHED
    
}
