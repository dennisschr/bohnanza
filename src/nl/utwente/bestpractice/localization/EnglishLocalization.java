package nl.utwente.bestpractice.localization;

import java.io.IOException;
import java.util.Properties;

public class EnglishLocalization extends Localization {
    
    private Properties properties;

    public EnglishLocalization() {
	
	try {
	    properties = new Properties();
	    properties.load(this.getClass().getResourceAsStream("/lang/english.prop"));
	} catch (IOException e) {
	    System.err.println("Unable to load english language file");
	    // TODO Auto-generated catch block
	    //e.printStackTrace();
	}
    }

    public String getString(String identifier) {
	return properties.getProperty(identifier, identifier);
	/*if(properties.containsKey(identifier)){
	    
	}
	if(identifier.equals(MainValues.FIELD_PLANT_INVALID_TYPE.toString())){
	    return "The bean you want to plant is not of the same type as the field";
	}else{
	    return identifier;
	}*/
    }

}
