package nl.utwente.bestpractice.localization;

public abstract class Localization {
    public abstract String getString(String identifier);  
    
    public String getString(Enum value){
	return this.getString(value.toString());
    }
}
