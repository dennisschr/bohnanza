package nl.utwente.bestpractice.action;

import nl.utwente.bestpractice.game.Player;

public abstract class Action {
    private Player player;

    public Action(Player player){
	this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
    
    //public abstract void execute();
}
