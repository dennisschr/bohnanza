package nl.utwente.bestpractice.action;

import java.util.Set;

import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.game.TradeRequest;
import nl.utwente.bestpractice.model.Card;

public class TradeAcceptAction extends Action {

    private final TradeRequest request;
    private final Set<Card> cards;

    /**
     * Accept a TradeRequest
     * @param player The player which accepts the TradeRequest
     * @param request The TradeRequest which is accepted
     * @param cards The cards of the player which are used in this trade. The cards do not need to be removed
     * beforehand, they are removed from the player when the trade is actually accepted.
     */
    public TradeAcceptAction(Player player, TradeRequest request, Set<Card> cards) {
	super(player);
	this.request = request;
	this.cards = cards;
    }

    public TradeRequest getRequest() {
        return request;
    }
    
    public Set<Card> getCards() {
	return cards;
    }
}
