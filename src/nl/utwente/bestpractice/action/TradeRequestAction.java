package nl.utwente.bestpractice.action;

import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.game.TradeRequest;

public class TradeRequestAction extends Action {

    private TradeRequest tradeRequest;

    public TradeRequestAction(Player player, TradeRequest tradeRequest) {
	super(player);
	this.tradeRequest = tradeRequest;
    }

    public TradeRequest getTradeRequest() {
	return this.tradeRequest;
    }
    
}
