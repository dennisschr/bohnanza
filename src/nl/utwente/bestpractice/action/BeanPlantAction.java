package nl.utwente.bestpractice.action;

import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Field;

public class BeanPlantAction extends Action {

    private final Field field;
    private final Card card;

    public BeanPlantAction(Player player, Field field, Card card) {
	super(player);
	this.field = field;
	this.card = card;
    }
    
    public Card getCard() {
	return card;
    }
    
    public Field getField() {
	return field;
    }
    
}
