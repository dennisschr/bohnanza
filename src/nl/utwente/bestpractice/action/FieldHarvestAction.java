package nl.utwente.bestpractice.action;

import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.model.Field;

public class FieldHarvestAction extends Action {

    private final Field field;

    public FieldHarvestAction(Player player, Field field) {
	super(player);
	this.field = field;
    }
    
    public Field getField() {
	return field;
    }
    
    
}
