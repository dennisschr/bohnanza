package nl.utwente.bestpractice.phase;

import java.util.List;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.exception.InvalidBeanTypeException;
import nl.utwente.bestpractice.game.AlCabohneForTwoGameDescription;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Field;
import nl.utwente.bestpractice.model.Hand;

public class GiveBeansToMafiaPhase implements Phase {
	
	Player currentPlayer;
	List<Field> playerFields;
	List<Field> mafiaField;
	List<BeanType> playerFieldsBeanType;
	List<BeanType> mafiaFieldBeantBeanType;
	List<BeanType> commonBeanType;

	public String getDescription() {
		String description = "In Al Cabohne extension, second phase of the game. The player gives a bean"
				+ "from his field if he has the same bean type as the mafia";
		return description;
	}

	public void init(Game game) {
		currentPlayer = game.getCurrentPlayer();
		playerFields = currentPlayer.getFields();
		for (int i=0; i<playerFields.size(); i++){
			playerFieldsBeanType.add(playerFields.get(i).getBeanType());
		}
		AlCabohneForTwoGameDescription gameDescription = (AlCabohneForTwoGameDescription) game.getDescription();
		mafiaField = gameDescription.getMafiaField();
		for (int i=0; i<mafiaField.size();i++){
			mafiaFieldBeantBeanType.add(mafiaField.get(i).getBeanType());
		}
	}

	public void start(Game game) {
		checkBeanType();
		//TODO : if common beantype, take one card from the concerned field of the player and  
		//put it in the mafia's field.
		
		//if there is at least one common bean type
		Hand currentHand = currentPlayer.getHand();
		while (!commonBeanType.isEmpty()){
			BeanType sharedBeanType = commonBeanType.get(0);
			commonBeanType.remove(0);
			
			Card givenToMafiaCard = currentHand.getCardByBeanType(sharedBeanType);
			if (mafiaField.get(0).getBeanType() == givenToMafiaCard.getBeanType()){
				AlCabohneForTwoGameDescription gameDescription = (AlCabohneForTwoGameDescription) game.getDescription();
				try {
				    gameDescription.getMafiaField().get(0).addCard(givenToMafiaCard);
				} catch (InvalidBeanTypeException e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				}
			}
			else{
				AlCabohneForTwoGameDescription gameDescription = (AlCabohneForTwoGameDescription) game.getDescription();
				try {
				    gameDescription.getMafiaField().get(1).addCard(givenToMafiaCard);
				} catch (InvalidBeanTypeException e) {
				    // TODO Auto-generated catch block
				    e.printStackTrace();
				}
			}
		}
	}

	/**
	 * check if there a common bean type between the player and mafia
	 */
	public void checkBeanType(){
		for (int i=0; i<playerFields.size(); i++){
			for (int j=0; j<mafiaField.size();j++){
				if (playerFieldsBeanType.get(i)==mafiaFieldBeantBeanType.get(j)){
					commonBeanType.add(playerFieldsBeanType.get(i));
				}
			}
		}
	}
	
	public void stop(Game game) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void onStart(Game game) {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onAfterStart(Game game) {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onAction(Action action) throws ActionNotAllowedException {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onStop(Game game) {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public List<Class<? extends Action>> getPossibleActions(Player player) {
	    // TODO Auto-generated method stub
	    return null;
	}

}
