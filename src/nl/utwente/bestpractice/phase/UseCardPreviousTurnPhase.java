package nl.utwente.bestpractice.phase;

import java.util.List;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.game.AlCabohneForTwoGameDescription;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.model.Card;

public class UseCardPreviousTurnPhase implements Phase {
	
	private List<List<Card>> cardsFromPrev ;
	
	public String getDescription() {
		String description = "In Al Cabohne extension. The player can use the cards left from previous player's turn";
		return description;
	}

	public void onStart(Game game) {
		Player currentPlayer = game.getCurrentPlayer();
		//we offer the cards to the current player
		while (! cardsFromPrev.isEmpty()){
			offerCardstoPlayer(currentPlayer, game);
			cardsFromPrev.remove(0);
		}
	}

	/**
	 * called to offer the cards from previous turn to the player.
	 * He can add them to oe of his fields or throw them in the discard pile
	 * @param player
	 * @param game
	 */
	private void offerCardstoPlayer(Player player, Game game) {
		List<Card> cards = cardsFromPrev.get(0);
		/*if (player.askPlayerInterest(cards)){
			//if player is interested in this cards
			List<Field> fields = player.getFields();
			for (int i=0; i<cards.size(); i++){
				for (int j=0; j<fields.size(); j++){
					try{
						fields.get(j).addCard(cards.get(i));
					}
					catch (IllegalArgumentException e){
					}
				}
			}
		}
		else {
			//player is not interested in the cards, we throw them away
			throwCardsToDiscard(cards, game);
		}*/
	}

	/**
	 * called to throw the refused card to the discard pile
	 * @param cards
	 * @param game
	 */
	private void throwCardsToDiscard(List<Card> cards, Game game) {
		for (int i=0; i< cards.size(); i++){
			game.getDiscardPile().addCard(cards.get(i));
		}
	}

	public void onStop(Game game) {
	}

	public boolean canStop() {
		boolean answer = false;
		if (cardsFromPrev.isEmpty()){
			answer = true;
		}
		return answer;
	}

	public void init(Game game) {
		AlCabohneForTwoGameDescription gameDescription = (AlCabohneForTwoGameDescription) game.getDescription();
		cardsFromPrev = gameDescription.getCardsFromPreviousTurn();		
	}

	@Override
	public String getName() {
	    // TODO Auto-generated method stub
	    return null;
	}

	@Override
	public void onAfterStart(Game game) {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onAction(Action action) throws ActionNotAllowedException {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public List<Class<? extends Action>> getPossibleActions(Player player) {
	    // TODO Auto-generated method stub
	    return null;
	}

}
