package nl.utwente.bestpractice.phase;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.action.BeanPlantAction;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.exception.InvalidBeanTypeException;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.localization.MainValues;

public class TradedBeansPlantPhase extends DefaultGameBasePhase {
    
    public TradedBeansPlantPhase() {
	super();
	
	this.possibleActionsCurrentPlayer.add(BeanPlantAction.class);
	this.possibleActionsOtherPlayer.add(BeanPlantAction.class);
    }
    
    public String getName(){
	return "Traded beans planting";
    }

    public String getDescription() {
	return "All players now plant the cards they received in trades or donations (the active player also plants any cards he drew and kept).";
    }

    @Override
    public void onAfterStart(Game game) {
        // TODO Auto-generated method stub
        super.onAfterStart(game);
        game.nextPhase();
    }
    

    public void onAction(BeanPlantAction action)
	    throws ActionNotAllowedException {
	// Check if the card is from the traded cards pile of the player
	if (action.getPlayer().getTradingArea().getTradedCards()
		.contains(action.getCard())) {
	    try {
		// Add card to the field
		action.getField().addCard(action.getCard());
		// Remove card from the traded cards
		action.getPlayer().getTradingArea().getTradedCards()
			.remove(action.getCard());

		// Check if all cards are planted
		// If so, we should move to the next phase
		boolean ready = true;
		for (Player player : game.getPlayers()) {
		    ready = ready
			    && player.getTradingArea().getTradedCards()
				    .isEmpty();
		}

		if (ready) {
		    // Move to the next phase
		    game.nextPhase();
		}

	    } catch (InvalidBeanTypeException e) {
		throw new ActionNotAllowedException(game.getLocalization()
			.getString(MainValues.FIELD_PLANT_INVALID_TYPE));
	    }
	}
    }

    public void onAction(Action action) throws ActionNotAllowedException {
	if (action instanceof BeanPlantAction) {
	    this.onAction((BeanPlantAction) action);
	} else if (action instanceof FieldHarvestAction) {
	    this.onAction((FieldHarvestAction) action);
	} else if (action instanceof FieldBuyAction) {
	    this.onAction((FieldBuyAction) action);
	} else {
	    throw new ActionNotAllowedException("Simply not allowed");
	}
    }

}
