package nl.utwente.bestpractice.phase;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.action.BeanPlantAction;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.action.PhaseStopAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.game.Game;

public class DrawCardsPhase extends DefaultGameBasePhase {
    
    private int number;

    public DrawCardsPhase(int number) {
	super();
	this.number = number;
    }

    @Override
    public String getName() {
        return "Draw cards";
    }
    
    public String getDescription() {
	return "The current player draws " + number + " card(s)";
    }

    public void onAfterStart(Game game) {
	super.onAfterStart(game);
	game.getCurrentPlayer().drawCard(game.getDeck(), this.number);
	game.nextPhase();
    }

    public void onAction(Action action) throws ActionNotAllowedException {
	if (action instanceof FieldHarvestAction) {
	    this.onAction((FieldHarvestAction) action);
	} else if (action instanceof FieldBuyAction) {
	    this.onAction((FieldBuyAction) action);
	} else{
	    throw new ActionNotAllowedException("Simply not allowed");
	}
    }

}
