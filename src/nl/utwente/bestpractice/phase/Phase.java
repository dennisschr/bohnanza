package nl.utwente.bestpractice.phase;

import java.util.List;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;

public interface Phase {

    public String getName();

    public String getDescription();
    
    /**
     * Initialize this phase for the given game
     * @param game
     */
    public void init(Game game);

    /**
     * Start this phase for the given game.
     * This method is called before the players know it starts.
     * The actual actions are executed in here.
     * game.nextPhase() should *NOT* be called in here
     * 
     * @param game
     */
    public void onStart(Game game);
    
    /**
     * Called after the phase is started and all other players know the phase is started
     * @param game
     */
    public void onAfterStart(Game game);
    
    public void onAction(Action action) throws ActionNotAllowedException;

    /**
     * Stop this phase
     * 
     * @param game
     */
    public void onStop(Game game);

    public List<Class<? extends Action>> getPossibleActions(Player player);
    
}
