package nl.utwente.bestpractice.phase;

import java.util.ArrayList;
import java.util.List;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.exception.MaximumFieldsCountReached;
import nl.utwente.bestpractice.exception.NotEnoughCoinsException;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.localization.MainValues;

public class DefaultGameBasePhase implements Phase {

    protected List<Class<? extends Action>> possibleActionsCurrentPlayer;
    protected List<Class<? extends Action>> possibleActionsOtherPlayer;
    protected Game game;

    public DefaultGameBasePhase() {
	this.possibleActionsCurrentPlayer = new ArrayList<Class<? extends Action>>();
	this.possibleActionsOtherPlayer = new ArrayList<Class<? extends Action>>();

	// These actions are always allowed
	this.possibleActionsCurrentPlayer.add(FieldHarvestAction.class);
	;
	this.possibleActionsCurrentPlayer.add(FieldBuyAction.class);
	this.possibleActionsCurrentPlayer.add(FieldHarvestAction.class);
	this.possibleActionsOtherPlayer.add(FieldBuyAction.class);
    }

    @Override
    public String getName() {
	return "Unknown";
    }

    @Override
    public String getDescription() {
	return "Unknown";
    }

    @Override
    public void init(Game game) {
	this.game = game;
    }

    @Override
    public void onStart(Game game) {
	// TODO Auto-generated method stub

    }

    @Override
    public void onStop(Game game) {
        // TODO Auto-generated method stub
    
    }

    public void onAction(FieldHarvestAction action)
	    throws ActionNotAllowedException {
	// Check if it is a field of the player
	if (action.getPlayer().getFields().contains(action.getField())) {
	    // Harvest
	    action.getField().harvest(action.getPlayer().getTreasury(),
		    game.getDiscardPile());
	}
    }
    
    public void onAction(FieldBuyAction action)
	    throws ActionNotAllowedException {
	    try {
		game.buyField(action.getPlayer());
	    } catch (NotEnoughCoinsException e) {
		throw new ActionNotAllowedException(this.game.getLocalization().getString(MainValues.FIELD_BUY_NOT_ENOUGH_COINS));
	    } catch (MaximumFieldsCountReached e) {
		throw new ActionNotAllowedException(this.game.getLocalization().getString(MainValues.FIELD_BUY_MAXIMUM_FIELDS_REACHED));
	    }
    }

    @Override
    public void onAction(Action action) throws ActionNotAllowedException {
	 throw new ActionNotAllowedException("You should not see this one");
    }

    @Override
    public List<Class<? extends Action>> getPossibleActions(Player player) {
	if (game == null)
	    return new ArrayList<Class<? extends Action>>();
	if (this.game.getCurrentPlayer().equals(player)) {
	    return this.possibleActionsCurrentPlayer;
	} else {
	    return this.possibleActionsOtherPlayer;
	}
    }

    @Override
    public void onAfterStart(Game game) {
	// TODO Auto-generated method stub
	
    }

}
