package nl.utwente.bestpractice.phase;

import java.util.ArrayList;
import java.util.List;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.action.BeanPlantAction;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.action.PhaseStopAction;
import nl.utwente.bestpractice.action.TradeAcceptAction;
import nl.utwente.bestpractice.action.TradeRequestAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.exception.InvalidCardsException;
import nl.utwente.bestpractice.exception.TradeAlreadyAcceptedException;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.game.Player;

public class TradePhase extends DefaultGameBasePhase {

    public TradePhase() {
	super();

	// Additional actions possible
	this.possibleActionsCurrentPlayer.add(TradeAcceptAction.class);
	this.possibleActionsCurrentPlayer.add(TradeRequestAction.class);
	this.possibleActionsCurrentPlayer.add(PhaseStopAction.class);
	
	this.possibleActionsOtherPlayer.add(TradeAcceptAction.class);
	this.possibleActionsOtherPlayer.add(TradeRequestAction.class);
    }

    @Override
    public String getName() {
	return "Trading";
    }

    public String getDescription() {
	return "The active player may trade cards with his fellow players. He may also offer to donate cards to them and receive offers for donated cards from them.";
    }

    public void onStart(Game game) {
	// The player draws two cards and puts them on the table
	//game.getCurrentPlayer().getTradingArea().putFaceUpCard(game.getDeck().drawCard());
	//game.getCurrentPlayer().getTradingArea().putFaceUpCard(game.getDeck().drawCard());
	

	// Move cards to trading area
	/*
	 * while (!game.getTable().getPile("trade").isEmpty()) {
	 * game.getCurrentPlayer().getTradingArea()
	 * .putFaceUpCard(game.getTable().getPile("trade").remove(0)); }
	 * game.getTable().removePile("trade");
	 */

	// while(!game.getCurrentPlayer().)

	// Ask each player for trade requests
	/*
	 * for (Player player : game.getPlayers()) {
	 * player.startTrade(game.getCurrentPlayer().getTradingArea()); }
	 */

	// TODO finish this

	// Loop through the requests
	// game.getCurrentPlayer().getTradingArea().g

	// At the end, face up cards go to the traded pile
	// game.getCurrentPlayer().getTradingArea().finishTrading();
    }

    public void onStop(Game game) {
	super.onStop(game);
	// Finish trading
	game.getCurrentPlayer().getTradingArea().finishTrading();
    }

    public void onAction(TradeRequestAction action) {
	game.getCurrentPlayer().getTradingArea()
		.addTradeRequest(action.getTradeRequest());
    }

    public void onAction(TradeAcceptAction action) {
	try {
	    action.getRequest().accept(action.getPlayer(), action.getCards());
	} catch (InvalidCardsException e) {
	    // TODO find some way to return this to the client
	} catch (TradeAlreadyAcceptedException e) {
	    // TODO find some way to return this to the client
	}
    }

    public void onAction(PhaseStopAction action) {
	game.nextPhase();
    }

    public void onAction(Action action) throws ActionNotAllowedException {
	if (action instanceof FieldBuyAction) {
	    this.onAction((FieldBuyAction) action);
	} else if (action instanceof FieldHarvestAction) {
	    this.onAction((FieldHarvestAction) action);
	} else if (action instanceof PhaseStopAction) {
	    this.onAction((PhaseStopAction) action);  
	} else if (action instanceof TradeRequestAction) {
	    this.onAction((TradeRequestAction) action);  
	} else if (action instanceof TradeAcceptAction) {
	    this.onAction((TradeAcceptAction) action);  
	} else {
	    throw new ActionNotAllowedException("Simply not allowed");
	}
    }

    @Override
    public List<Class<? extends Action>> getPossibleActions(Player player) {
	if (game == null)
	    return new ArrayList<Class<? extends Action>>();
	if (this.game.getCurrentPlayer().equals(player)) {
	    return this.possibleActionsCurrentPlayer;
	} else {
	    return this.possibleActionsOtherPlayer;
	}
    }

}
