package nl.utwente.bestpractice.phase;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.action.BeanPlantAction;
import nl.utwente.bestpractice.action.FieldBuyAction;
import nl.utwente.bestpractice.action.FieldHarvestAction;
import nl.utwente.bestpractice.action.PhaseStopAction;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.exception.InvalidBeanTypeException;
import nl.utwente.bestpractice.game.Game;
import nl.utwente.bestpractice.localization.MainValues;

public class BeansPlantPhase extends DefaultGameBasePhase {

    private int planted;

    public BeansPlantPhase() {
	super();
	
	// Additional actions possible
	this.possibleActionsCurrentPlayer.add(BeanPlantAction.class);
	this.possibleActionsCurrentPlayer.add(PhaseStopAction.class);;
    }

    @Override
    public String getName() {
	return "Bean planting";
    }

    public String getDescription() {
	return "The player must plant the first card in its hand, and may plant a second card";
    }

    public void init(Game game) {
	super.init(game);
	this.planted = 0;
    }

    public void onAction(BeanPlantAction action)
	    throws ActionNotAllowedException {
	if (planted == 2)
	    throw new ActionNotAllowedException(this.game.getLocalization()
		    .getString(MainValues.FIELD_PLANT_PHASE_MAXIMUM_REACHED));

	// Check if the card is the first of the hand
	if (action.getPlayer().getHand().getCards().peek()
		.equals(action.getCard())) {
	    try {
		// Add card to the field
		// Remove card from the hand
		action.getField().addCard(action.getPlayer().getHand().getCard());

		// Increase counter
		this.planted++;
	    } catch (InvalidBeanTypeException e) {
		throw new ActionNotAllowedException(game.getLocalization()
			.getString(MainValues.FIELD_PLANT_INVALID_TYPE));
	    }
	}else{
	    throw new ActionNotAllowedException(game.getLocalization()
			.getString(MainValues.HAND_ONLY_FIRST));
	}
    }

    public void onAction(PhaseStopAction action)
	    throws ActionNotAllowedException {
	if (planted == 0)
	    throw new ActionNotAllowedException(this.game.getLocalization()
		    .getString(MainValues.FIELD_PLANT_PHASE_MINIMUM_NEEDED));

	game.nextPhase();
    }

    public void onAction(Action action) throws ActionNotAllowedException {
	if (action instanceof BeanPlantAction) {
	    this.onAction((BeanPlantAction) action);
	} else if (action instanceof FieldHarvestAction) {
	    this.onAction((FieldHarvestAction) action);
	} else if (action instanceof FieldBuyAction) {
	    this.onAction((FieldBuyAction) action);
	} else if (action instanceof PhaseStopAction) {
	    this.onAction((PhaseStopAction) action);
	} else {
	    throw new ActionNotAllowedException("Simply not allowed");
	}
    }
}
