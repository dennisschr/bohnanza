package nl.utwente.bestpractice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

public class Deck extends Observable {

    List<Card> cards;

    public Deck() {
	this.cards = new ArrayList<Card>();
    }

    /**
     * Draw a card from the deck
     * 
     * @return
     */
    public Card drawCard() {
	// Get card from the end of the list
	Card c =  this.cards.isEmpty() ? null : this.cards.remove(this.cards
		.size() - 1);

	// Notify observers
	this.setChanged();
	this.notifyObservers();
	
	return c;
    }

    /**
     * Refill the deck from the discard pile. This method also empties the
     * discard pile and shuffles the cards.
     * 
     * @param listCards
     */
    public void fromDiscardPile(DiscardPile discardPile) {
	this.addCards(discardPile.getCards());
	discardPile.empty();

	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Add the given cards to the deck and shuffle the deck afterwards
     * 
     * @param cards
     */
    public void addCards(List<Card> cards) {
	Random random = new Random();
	while (cards.size() > 0) {
	    this.cards.add(cards.remove(random.nextInt(cards.size())));
	}

	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * return true if the deck is empty
     * 
     * @return
     */
    public boolean isEmpty() {
	return this.cards.isEmpty();
    }

    public int size() {
	return this.cards.size();
    }
}
