package nl.utwente.bestpractice.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Table {
    private Map<String, List<Card>> piles;
    
    public Table(){
	this.piles = new HashMap<String, List<Card>>();
    }
    
    public List<Card> getPile(String identifier){
	return this.piles.get(identifier);
    }
    
    public void createPile(String identifier){
	this.createPile(identifier, new ArrayList<Card>());
    }

    public void createPile(String identifier, ArrayList<Card> list) {
	this.piles.put(identifier, list);
    }
    
    public void addCardToPile(String identifier, Card card){
	if(!this.pileExists(identifier)) this.createPile(identifier);
	
	this.getPile(identifier).add(card);
    }

    private boolean pileExists(String identifier) {
	return this.piles.containsKey(identifier);
    }
    
    public List<Card> removePile(String identifier) {
	return this.piles.remove(identifier);
    }
}
