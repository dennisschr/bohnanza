package nl.utwente.bestpractice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import nl.utwente.bestpractice.exception.InvalidBeanTypeException;

public class Field extends Observable {
    List<Card> cards;

    public Field() {
	this.cards = new ArrayList<Card>();
    }

    /**
     * Method to harvest the considered field
     */
    public void harvest(Treasury treasury, DiscardPile discardPile) {
	int amount = this.getBeanCount();
	int coins = this.getBeanType().getCoinNumber(amount);

	// Move cards to correct location
	// First coins cards are moved to treasury
	// Remaining cards are moved to discard pile
	for (int i = 0; i < amount; i++) {
	    if (i < coins) {
		// Add card to treasury
		treasury.addCoin(this.cards.get(i));
	    } else {
		// Add card to discard pile
		discardPile.addCard(this.cards.get(i));
	    }
	}

	// Clear field
	this.cards.clear();

	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    public int getBeanCount() {
	return this.cards.size();
    }

    /**
     * @param card
     * @throws InvalidBeanTypeException
     */
    public void addCard(Card card) throws InvalidBeanTypeException {
	BeanType type = this.getBeanType();
	if (type != null && !type.equals(card.getBeanType())) {
	    throw new InvalidBeanTypeException();
	}
	this.cards.add(card);

	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * return the bean type of the cards in the field
     * 
     * @return
     */
    public BeanType getBeanType() {
	return this.cards.isEmpty() ? null : this.cards.get(0).getBeanType();
    }

    public boolean assertBeanType(BeanType beantype) {
	if (beantype == this.getBeanType()) {
	    return true;
	}
	return false;
    }

    public List<Card> getCards() {
	return this.cards;
    }
}
