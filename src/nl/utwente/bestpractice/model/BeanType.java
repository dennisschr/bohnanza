package nl.utwente.bestpractice.model;

import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

public class BeanType {
    String name;
    int amount;
    SortedMap<Integer, Integer> beanometer;

    public BeanType(String name) {
	this.name = name;
	this.beanometer = new TreeMap<Integer, Integer>();
    }

    /**
     * Set the beanometer of this beantype
     * 
     * @param beanometer
     */
    /*
     * public void setBeanometer(SortedMap<Integer, Integer> beanometer) {
     * this.beanometer = beanometer; }
     */

    /**
     * Add a new value to the BeanOMeter
     * 
     * @param numberToHarvest
     *            The number of beans to harvest
     * @param numberOfCoins
     *            The amount of coins retreived for this number of beans
     */
    public void setBeanometerValue(int numberToHarvest, int numberOfCoins) {
	this.beanometer.put(numberToHarvest, numberOfCoins);
    }
    
    public SortedMap<Integer, Integer> getBeanometer() {
	return beanometer;
    }

    /**
     * return the number of coins you earn with the number of cards you harvest
     * of your field
     * 
     * @return
     */
    public int getCoinNumber(int cardNumber) {
	int amount = 0;
	// We can make use of the sorted nature of the map here
	for (Entry<Integer, Integer> entry : this.beanometer.entrySet()) {
	    if (entry.getKey() <= cardNumber)
		amount = entry.getValue();
	}
	return amount;
    }

    /**
     * return the name of the Bean type
     * 
     * @return
     */
    public String getName() {
	return name;
    }

    /**
     * return the number of cards you ca find oh this type in the game
     * 
     * @return
     */
    public int getAmount() {
	return amount;
    }

    public void setAmount(int amount) {
	this.amount = amount;
    }
    
    @Override
    public String toString() {
        return this.name + " " + this.beanometer.toString() + " " + this.amount;
    }
}
