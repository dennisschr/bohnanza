package nl.utwente.bestpractice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.game.TradeRequest;

public class TradingArea extends Observable {

    Player player;
    List<Card> tradedCards;
    List<Card> faceUpCards;
    List<TradeRequest> requests;

    /**
     * Create a new TradingArea for the given Player
     * 
     * @param player
     *            The player to whom this TradingArea belongs
     */
    public TradingArea(Player player) {
	this.player = player;
	this.tradedCards = new ArrayList<Card>();
	this.faceUpCards = new ArrayList<Card>();
	this.requests = new ArrayList<TradeRequest>();
    }

    /**
     * Put a card faceup on the table for all players to be seen
     * 
     * @param card
     */
    public void putFaceUpCard(Card card) {
	this.faceUpCards.add(card);

	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Put a card on a pile of traded cards. All traded cards should be planted
     * in the next round
     * 
     * @param card
     */
    public void putTradedCard(Card card) {
	this.tradedCards.add(card);
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Finish trading by moving all cards to the traded cards pile
     */
    public void finishTrading() {
	this.tradedCards.addAll(this.faceUpCards);
	this.faceUpCards.clear();
	this.clearTradeRequest();
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Remove a card from the trading area (used when planting cards)
     * 
     * @param card
     */
    public void removeTradedCard(Card card) {
	this.tradedCards.remove(card);
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Add a new trade request
     * 
     * @param tradeReq
     */
    public void addTradeRequest(TradeRequest tradeReq) {
	this.requests.add(tradeReq);
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Remove a trade request
     * 
     * @param tradeReq
     */
    public void removeTradeRequest(TradeRequest tradeReq) {
	this.requests.remove(tradeReq);
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * Remove all trade requests
     */
    public void clearTradeRequest() {
	this.requests.clear();
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    public List<Card> getFaceUpCards() {
	return faceUpCards;
    }

    public List<Card> getTradedCards() {
	return tradedCards;
    }

    public Player getPlayer() {
	return player;
    }

    public List<TradeRequest> getRequests() {
	return requests;
    }

    public void clearTradedCards() {
	this.tradedCards.clear();
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }
}
