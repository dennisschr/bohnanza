package nl.utwente.bestpractice.model;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Observable;
import java.util.Queue;

public class Hand extends Observable {
    Deque<Card> cards;

    public Hand() {
	this.cards = new ArrayDeque<Card>();
    }

    /**
     * Add a card to the hand
     */
    public void addCard(Card card) {
	this.cards.add(card);
	this.changed();
    }

    /**
     * Add a card to the hand, but on the front
     * 
     * @param card
     */
    public void addCardToFront(Card card) {
	this.cards.addFirst(card);
	this.changed();
    }

    /**
     * used to get the first card of the hand
     * 
     * @return
     */
    public Card getCard() {
	Card card = this.cards.poll();
	this.changed();
	return card;
    }

    public Queue<Card> getCards() {
	return this.cards;
    }

    /**
     * to get a card in the hand according to its bean type
     * 
     * @param beanType
     * @return the first card of the hand of the considered bean type
     */
    public Card getCardByBeanType(BeanType beanType) {
	Deque<Card> newHand = new ArrayDeque<Card>();
	Card observeCard;
	Card returnedCard = null;
	while (returnedCard == null) {
	    for (int i = 0; i < getCards().size(); i++) {
		observeCard = getCard();
		if (observeCard.getBeanType() == beanType) {
		    returnedCard = observeCard;
		} else {
		    newHand.add(observeCard);
		}
	    }
	}
	// Putting all the rest of the hand into the new hand
	newHand.addAll(getCards());
	// put back the left cards in the hand
	cards.addAll(newHand);

	this.changed();

	return returnedCard;
    }

    private void changed() {
	this.setChanged();
	this.notifyObservers();
    }
}
