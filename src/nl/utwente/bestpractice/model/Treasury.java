package nl.utwente.bestpractice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Treasury extends Observable {
    private List<Card> listOfCards;

    /**
     * Constructor
     */
    public Treasury() {
	listOfCards = new ArrayList<Card>();
    }

    public int getCoins() {
	return this.listOfCards.size();
    }

    public List<Card> getListOfCards() {
	return listOfCards;
    }

    /**
     * remove the first card from the treasury's player
     * 
     * @return the card removed from the treasury
     */
    public Card removeCoin() {
	if (getListOfCards().isEmpty()) {
	    return null;
	}
	Card c = getListOfCards().remove(0);

	// Notify observers
	this.setChanged();
	this.notifyObservers();
	
	return c;
    }

    /**
     * to add a card in the treasury's player
     */
    public void addCoin(Card card) {
	this.listOfCards.add(card);

	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    public void removeCoins(int amount) {
	for (int i = 0; i < amount; i++) {
	    this.removeCoin();
	}
    }
}
