package nl.utwente.bestpractice.model;

public class Card {
    BeanType beanType;

    public Card(BeanType beanType) {
	this.beanType = beanType;
	if (beanType == null) {
	    throw new IllegalArgumentException();
	}
    }

    /**
     * return the bean type of the considered card
     * 
     * @return
     */
    public BeanType getBeanType() {
	return this.beanType;
    }
    
    @Override
    public String toString() {
	return this.beanType.toString();
    }
}
