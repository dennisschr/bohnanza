package nl.utwente.bestpractice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class DiscardPile extends Observable {
    List<Card> listOfCards;

    public DiscardPile() {
	this.listOfCards = new ArrayList<Card>();
    }

    /**
     * return the card on the top of the Discard Pile
     * 
     * @return
     */
    public Card getTopCard() {
	return this.listOfCards.isEmpty() ? null : this.listOfCards
		.get(this.listOfCards.size() - 1);
    }

    /**
     * add a card to the discard pile
     */
    public void addCard(Card card) {
	this.listOfCards.add(card);
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }

    /**
     * return a list of the cards that were in the discard pile to empty it
     * (when the deck is empty)
     * 
     * @return
     */
    public List<Card> getCards() {
	return this.listOfCards;
    }

    public void empty() {
	this.listOfCards.clear();
	// Notify observers
	this.setChanged();
	this.notifyObservers();
    }
}
