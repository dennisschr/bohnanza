package nl.utwente.bestpractice.exception;

public class GameException extends Exception {
    
    public GameException(){
	
    }

    public GameException(String message) {
	super(message);
    }

}
