package nl.utwente.bestpractice.exception;

public class ActionNotAllowedException extends GameException {

    public ActionNotAllowedException() {

    }

    public ActionNotAllowedException(String message) {
	super(message);
    }

}
