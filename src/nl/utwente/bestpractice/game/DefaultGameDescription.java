package nl.utwente.bestpractice.game;

import java.util.ArrayList;
import java.util.List;

import nl.utwente.bestpractice.localization.EnglishLocalization;
import nl.utwente.bestpractice.localization.Localization;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Deck;
import nl.utwente.bestpractice.model.DiscardPile;
import nl.utwente.bestpractice.model.Field;
import nl.utwente.bestpractice.model.Table;
import nl.utwente.bestpractice.phase.BeansPlantPhase;
import nl.utwente.bestpractice.phase.DrawCardsPhase;
import nl.utwente.bestpractice.phase.Phase;
import nl.utwente.bestpractice.phase.TradePhase;
import nl.utwente.bestpractice.phase.TradedBeansPlantPhase;

public class DefaultGameDescription extends GameDescription {

    private List<Card> cards;
    private List<Phase> phases;
    
    public String getName(){
	return "Default game";
    }

    public int getMinimumPlayers() {
	return 3;
    }

    public int getMaximumPlayers() {
	return 7;
    }

    public List<Card> getCards() {
	if (this.cards == null) {
	    this.cards = new ArrayList<Card>();

	    // Based on http://en.wikipedia.org/wiki/Bohnanza#Cards
	    BeanType coffee = new BeanType("Coffee");
	    coffee.setBeanometerValue(4, 1);
	    coffee.setBeanometerValue(7, 2);
	    coffee.setBeanometerValue(10, 3);
	    coffee.setBeanometerValue(12, 4);
	    coffee.setAmount(24);

	    BeanType wax = new BeanType("Wax");
	    wax.setBeanometerValue(4, 1);
	    wax.setBeanometerValue(7, 2);
	    wax.setBeanometerValue(9, 3);
	    wax.setBeanometerValue(11, 4);
	    wax.setAmount(22);

	    BeanType blue = new BeanType("Blue");
	    blue.setBeanometerValue(4, 1);
	    blue.setBeanometerValue(6, 2);
	    blue.setBeanometerValue(8, 3);
	    blue.setBeanometerValue(10, 4);
	    blue.setAmount(20);

	    BeanType chili = new BeanType("Chili");
	    chili.setBeanometerValue(3, 1);
	    chili.setBeanometerValue(6, 2);
	    chili.setBeanometerValue(8, 3);
	    chili.setBeanometerValue(9, 4);
	    chili.setAmount(18);

	    BeanType stink = new BeanType("Stink");
	    stink.setBeanometerValue(3, 1);
	    stink.setBeanometerValue(5, 2);
	    stink.setBeanometerValue(7, 3);
	    stink.setBeanometerValue(8, 4);
	    stink.setAmount(16);

	    BeanType green = new BeanType("Green");
	    green.setBeanometerValue(3, 1);
	    green.setBeanometerValue(5, 2);
	    green.setBeanometerValue(6, 3);
	    green.setBeanometerValue(7, 4);
	    green.setAmount(14);

	    BeanType soy = new BeanType("Soy");
	    soy.setBeanometerValue(2, 1);
	    soy.setBeanometerValue(4, 2);
	    soy.setBeanometerValue(6, 3);
	    soy.setBeanometerValue(7, 4);
	    soy.setAmount(12);

	    BeanType blackEyed = new BeanType("Black-eyed");
	    blackEyed.setBeanometerValue(2, 1);
	    blackEyed.setBeanometerValue(4, 2);
	    blackEyed.setBeanometerValue(5, 3);
	    blackEyed.setBeanometerValue(6, 4);
	    blackEyed.setAmount(10);

	    BeanType red = new BeanType("Red");
	    red.setBeanometerValue(2, 1);
	    red.setBeanometerValue(3, 2);
	    red.setBeanometerValue(4, 3);
	    red.setBeanometerValue(5, 4);
	    red.setAmount(8);

	    BeanType garden = new BeanType("Garden");
	    garden.setBeanometerValue(2, 2);
	    garden.setBeanometerValue(3, 3);
	    garden.setAmount(6);

	    BeanType cocoa = new BeanType("Cocoa");
	    cocoa.setBeanometerValue(2, 2);
	    cocoa.setBeanometerValue(3, 3);
	    cocoa.setBeanometerValue(4, 4);
	    cocoa.setAmount(4);

	    this.cards.addAll(this.beanTypeToCards(coffee));
	    this.cards.addAll(this.beanTypeToCards(wax));
	    this.cards.addAll(this.beanTypeToCards(blue));
	    this.cards.addAll(this.beanTypeToCards(chili));
	    this.cards.addAll(this.beanTypeToCards(stink));
	    this.cards.addAll(this.beanTypeToCards(green));
	    this.cards.addAll(this.beanTypeToCards(soy));
	    this.cards.addAll(this.beanTypeToCards(blackEyed));
	    this.cards.addAll(this.beanTypeToCards(red));
	    this.cards.addAll(this.beanTypeToCards(garden));
	    this.cards.addAll(this.beanTypeToCards(cocoa));

	}

	return this.cards;
    }

    public List<Phase> getPhases() {
	if (this.phases == null) {
	    this.phases = new ArrayList<Phase>();
	    this.phases.add(new BeansPlantPhase());
	    this.phases.add(new TradePhase());
	    this.phases.add(new TradedBeansPlantPhase());
	    this.phases.add(new DrawCardsPhase(3));
	}

	return this.phases;
    }

    @Override
    public int getNumberOfStartingCards() {
	return 5;
    }

    @Override
    public int getNumberOfStartingFields() {
	return 2;
    }

    @Override
    public int getMaximumFields() {
	return 3;
    }

    @Override
    public int getFieldCost() {
	return 3;
    }

    @Override
    public Player createPlayer(String name) {
	return new Player(name);
    }

    @Override
    public Field createField() {
	return new Field();
    }

    @Override
    public Deck createDeck() {
        return new Deck();
    }

    @Override
    public DiscardPile createDiscardPile() {
        return new DiscardPile();
    }
    
    @Override
    public Table createTable() {
        return new Table();
    }

    @Override
    public boolean isFinished(Game game) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public Localization getLocalization() {
	return new EnglishLocalization();
    }
}
