package nl.utwente.bestpractice.game;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.utwente.bestpractice.exception.InvalidCardsException;
import nl.utwente.bestpractice.exception.TradeAlreadyAcceptedException;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;

public class TradeRequest {
	Player player;
	Set<Card> cards;
	Map<BeanType, Integer> wanted;
	private boolean accepted;

	/**
	 * Create a new TradeRequest. A TradeRequest indicates that the given player
	 * is willing to give the given cards for the given wanted card types
	 * 
	 * @param player
	 *            The player willing to trade
	 * @param cards
	 *            The cards this player offers in the trade
	 * @param wanted
	 *            The cards this player wants in return
	 */
	public TradeRequest(Player player, Set<Card> cards,
			Map<BeanType, Integer> wanted) {
		this.player = player;
		this.cards = cards;
		this.wanted = wanted;
		this.accepted = false;
	}

	/**
	 * Accept this TradeRequest
	 * 
	 * @param player
	 *            The accepting player
	 * @param cards
	 *            The wanted cards
	 */
	public void accept(Player player, Set<Card> cards)
			throws InvalidCardsException, TradeAlreadyAcceptedException {
		// Check if this request is still open
		if (this.accepted)
			throw new TradeAlreadyAcceptedException();

		// Count the cards in the set
		Map<BeanType, Integer> actual = new HashMap<BeanType, Integer>();
		for (Card card : cards) {
			actual.put(card.getBeanType(), actual.containsKey(card
					.getBeanType()) ? actual.get(card.getBeanType()) + 1 : 1);
		}

		if (actual.equals(this.wanted)) {
			// Ok
			this.accepted = true;
			// Trade cards
			// TODO check if all cards are removed?
			this.removeCards(this.player, this.cards);
			this.removeCards(player, cards);
			this.addCards(this.player, cards);
			this.addCards(player, this.cards);
		} else {
			// Card not correct
			throw new InvalidCardsException();
		}
	}

	/**
	 * Add the given cards to the traded cards pile of the given player
	 * 
	 * @param player
	 * @param cards
	 */
	private void addCards(Player player, Set<Card> cards) {
		player.getTradingArea().getTradedCards().addAll(cards);
	}

	/**
	 * Remove the given cards from the given player. The cards are as much as
	 * possible removed from the trading area (only face up cards), the
	 * remainder is taken from the hand
	 * 
	 * @param player
	 * @param cards
	 * @return boolean Whether all cards have been successfully removed
	 */
	private boolean removeCards(Player player, Set<Card> cards) {
		int removed = 0;
		List<Card> faceUpCards = player.getTradingArea().getFaceUpCards();
		for (Card card : cards) {
			if (faceUpCards.remove(card))
				removed++;
		}
		for (Card card : cards) {
			if (player.getHand().getCards().remove(card))
				removed++;
		}
		return removed == cards.size();
	}

	public Set<Card> getCards() {
		return cards;
	}

	public Player getPlayer() {
		return player;
	}

	public Map<BeanType, Integer> getWanted() {
		return wanted;
	}

	public boolean isAccepted() {
		return this.accepted;
	}
}
