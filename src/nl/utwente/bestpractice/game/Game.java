package nl.utwente.bestpractice.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import nl.utwente.bestpractice.action.Action;
import nl.utwente.bestpractice.exception.ActionNotAllowedException;
import nl.utwente.bestpractice.exception.GameAlreadyStartedException;
import nl.utwente.bestpractice.exception.InvalidPlayerCountException;
import nl.utwente.bestpractice.exception.MaximumFieldsCountReached;
import nl.utwente.bestpractice.exception.NotEnoughCoinsException;
import nl.utwente.bestpractice.localization.Localization;
import nl.utwente.bestpractice.model.Deck;
import nl.utwente.bestpractice.model.DiscardPile;
import nl.utwente.bestpractice.model.Field;
import nl.utwente.bestpractice.model.Table;
import nl.utwente.bestpractice.model.TradingArea;
import nl.utwente.bestpractice.phase.Phase;

/**
 * The Game class acts as the director/mediator of the game.
 */
public class Game extends Observable implements Observer{
    public enum ObservableChange {
	PLAYER_TURN, PHASE, PLAYERS, DECK, DISCARDPILE, SHUFFLES
    }

    private List<Player> players;
    private Deck deck;
    private DiscardPile discardPile;
    int shuffles;
    private TradingArea activeTradeArea;
    private boolean started;

    private int currentPlayerIndex;
    private int currentPhaseIndex;

    private GameDescription gameDescription;
    private Table table;

    public Game(GameDescription gameDescription) {
	this.gameDescription = gameDescription;
	this.players = new ArrayList<Player>();
	this.deck = gameDescription.createDeck();
	this.discardPile = gameDescription.createDiscardPile();
	
	this.deck.addObserver(this);
	this.discardPile.addObserver(this);
	
	this.table = gameDescription.createTable();
	this.shuffles = 0;
    }

    /**
     * Add a Player to this game
     * 
     * @param player The player to add to this game
     * @throws GameAlreadyStartedException If the game has already started and it is not possible to add players
     */
    public void addPlayer(Player player) throws GameAlreadyStartedException {
	if (this.started)
	    throw new GameAlreadyStartedException();
	this.players.add(player);
	player.setGame(this);
	
	this.notify(ObservableChange.PLAYERS);
    }
    
    /**
     * Remove a Player from this game
     * @param player The player to remove
     * @throws GameAlreadyStartedException If the game has already started and it is not possible to remove the player
     */
    public void removePlayer(Player player) throws GameAlreadyStartedException{
	if (this.started)
	    throw new GameAlreadyStartedException();
	this.players.remove(player);
	player.setGame(null);
	
	this.notify(ObservableChange.PLAYERS);
    }

    public List<Player> getPlayers() {
	return players;
    }

    /**
     * Start the game
     */
    public void start() throws InvalidPlayerCountException {
	if (players.size() < this.gameDescription.getMinimumPlayers()
		|| players.size() > this.gameDescription.getMaximumPlayers()) {
	    throw new InvalidPlayerCountException();
	} else {
	    this.started = true;
	 
	    // Preparation
	    // Put all cards in the deck
	    this.deck.addCards(this.gameDescription.getCards());
	    
	    // Give each player his/her fields
	    for (Player player : this.players) {
		for(int i = 0; i < this.gameDescription.getNumberOfStartingFields(); i++){
		    player.addField(this.gameDescription.createField());
		}
	    }

	    // Make all players draw cards
	    for (Player player : this.players) {
		player.drawCard(this.getDeck(), this.gameDescription.getNumberOfStartingCards());
	    }

	    // If needed let the GameDescription perform some action
	    this.gameDescription.onGameStart(this);

	    // Set the phase to the starting phase
	    this.currentPhaseIndex = this.gameDescription.getStartingPhase();

	    // Give Turn to the first player
	    this.getCurrentPlayer().takeTurn();
	    
	    this.initPhase();
	    this.startPhase();
	    
	    // Run the game
	   /* while(!this.gameDescription.isFinished(this)){
	    	
	    	this.stopPhase();
	    	this.nextPhase();
	    }*/
	    
	    // Game is finished
	}
    }

    /**
     * Give the turn to the next player
     */
    public void nextPlayer() {
	System.out.println("Player endTurn " + this.getCurrentPlayer());
	// Notify that the turn for the current player has ended
	this.getCurrentPlayer().endTurn();
	// Move to the nex player
	this.currentPlayerIndex = (this.currentPlayerIndex + 1)
		% this.players.size();
	// Notify that his/her turn has started
	this.notify(ObservableChange.PLAYER_TURN);
	
	System.out.println("Player takeTurn " + this.getCurrentPlayer());
	this.getCurrentPlayer().takeTurn();
    }
    
    /**
     * Move the game to the next phase and, if this is the last phase of a
     * round, move to the next player
     */
    public void nextPhase() {
	// End the current phase
	this.stopPhase();

	// Go to the next phase
	this.currentPhaseIndex = (this.currentPhaseIndex + 1)
		% this.gameDescription.getPhases().size();

	// If this is the first phase, it is the turn of the next player
	if (this.currentPhaseIndex == 0) {
	    this.nextPlayer();
	}

	// Start the next phase
	this.initPhase();
	this.startPhase();
    }

    /**
     * Start the current phase
     */
    public void initPhase() {
	System.out.println("Phase init " + this.getCurrentPhase());
	this.getCurrentPhase().init(this);
        for(Player player: this.getPlayers()){
            player.onPhaseInit(this.getCurrentPhase());
        }
        this.notify(ObservableChange.PHASE);
    }
    
    public void startPhase() {
	System.out.println("Phase start " + this.getCurrentPhase());
	this.getCurrentPhase().onStart(this);
	for(Player player: this.getPlayers()){
            player.onPhaseStart(this.getCurrentPhase());
        }
	System.out.println("Phase after start " + this.getCurrentPhase());
	this.getCurrentPhase().onAfterStart(this);
    }

    /**
     * Stop the current phase
     */
    public void stopPhase() {
	System.out.println("Phase stop " + this.getCurrentPhase());
	this.getCurrentPhase().onStop(this);
	for(Player player: this.getPlayers()){
	    player.onPhaseStop(this.getCurrentPhase());
	}
    }
    
    /**
     * Submit an Action to the game
     * @param action
     * @throws ActionNotAllowedException 
     */
    public void submitAction(Action action) throws ActionNotAllowedException{
	System.out.println("Action " + action);
	this.getCurrentPhase().onAction(action);
    }

    public Player getCurrentPlayer() {
	return this.players.get(this.currentPlayerIndex);
    }

    public Phase getCurrentPhase() {
	return this.gameDescription.getPhases().get(this.currentPhaseIndex);
    }

    /**
     * Called by a Player (current turn) to indicate that it wants to start
     * trading
     * 
     * @param tradingArea
     *            The TradingArea of the Player
 *       @deprecated should be in phase
     */
    
    /*public void startTrade(TradingArea tradingArea) {
	this.activeTradeArea = tradingArea;
	for (Player player : players) {
	    player.startTrade(tradingArea);
	}
    }*/

    /**
     * Called by a Player (current turn) to indicate that it wants to stop
     * trading
     * @deprecated should be in phase
     */
    /*public void stopTrade() {
	this.activeTradeArea.finishTrading();
	for (Player player : players) {
	    player.stopTrade();
	}
	// finish trading area
	// notify players in order to plant beans
    }*/

    /**
     * The given Player wants to buy a field
     * 
     * @param player
     * @return
     */
    public void buyField(Player player) throws NotEnoughCoinsException,
	    MaximumFieldsCountReached {
	if (player.getTreasury().getCoins() < this.gameDescription.getFieldCost()) {
	    throw new NotEnoughCoinsException();
	} else if (player.getFields().size() == this.gameDescription.getMaximumFields()) {
	    throw new MaximumFieldsCountReached();
	} else {
	    player.getTreasury().removeCoins(this.gameDescription.getFieldCost());
	    player.getFields().add(new Field());
	}
    }

    public GameDescription getDescription() {
	return this.gameDescription;
    }

    public Deck getDeck() {
        return deck;
    }

    public DiscardPile getDiscardPile() {
        return discardPile;
    }

    /**
     * Refill the deck
     */
    public void refillDeck() {
        this.getDeck().fromDiscardPile(this.discardPile);
        this.shuffles++;
        
        this.notify(ObservableChange.DECK);
        this.notify(ObservableChange.DISCARDPILE);
        this.notify(ObservableChange.SHUFFLES);
    }

    public int getShuffles() {
        return shuffles;
    }

    public boolean isStarted() {
        return started;
    }

    public Table getTable() {
	return this.table;
    }

    /**
     * Return a Localization class responsible for the correct localization (translation) of displayed Strings
     * @return The Localization class responsible for the localization
     */
    public Localization getLocalization() {
	return this.gameDescription.getLocalization();
    }

    private void notify(ObservableChange change) {
        this.setChanged();
        this.notifyObservers(change);
    }

    @Override
    public void update(Observable observable, Object data) {
	if(observable.equals(this.deck)){
	    notify(ObservableChange.DECK);
	    // Refill deck if needed
	    if(this.deck.size() == 0){
		this.refillDeck();
	    }
	}else if(observable.equals(this.discardPile)){
	    notify(ObservableChange.DISCARDPILE);
	}
    }
}
