package nl.utwente.bestpractice.game;

import java.util.ArrayList;
import java.util.List;

import nl.utwente.bestpractice.localization.EnglishLocalization;
import nl.utwente.bestpractice.localization.Localization;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Deck;
import nl.utwente.bestpractice.model.DiscardPile;
import nl.utwente.bestpractice.model.Field;
import nl.utwente.bestpractice.model.Table;
import nl.utwente.bestpractice.phase.Phase;

public class AlCabohneForTwoGameDescription extends GameDescription {

    private List<Card> cards;
    private List<Phase> phases;
    private List<List<Card>> cardsFromPreviousTurn;
    protected List<Field> mafiaField;

    public String getName() {
	return "Al Cabohne 2 players";
    }

    public AlCabohneForTwoGameDescription() {
	// TODO Auto-generated constructor stub
    }

    @Override
    public int getMinimumPlayers() {
	return 2;
    }

    @Override
    public int getMaximumPlayers() {
	return 2;
    }

    @Override
    public List<Card> getCards() {
	// TODO Auto-generated method stub
	// if null new arrawList<Card>
	// then return this.card

	if (this.cards == null) {
	    this.cards = new ArrayList<Card>();

	    BeanType blue = new BeanType("Blue");
	    blue.setBeanometerValue(4, 1);
	    blue.setBeanometerValue(6, 2);
	    blue.setBeanometerValue(8, 3);
	    blue.setBeanometerValue(10, 4);
	    blue.setAmount(20);

	    BeanType kidney = new BeanType("Kidney");
	    kidney.setBeanometerValue(3, 1);
	    kidney.setBeanometerValue(6, 2);
	    kidney.setBeanometerValue(7, 3);
	    kidney.setBeanometerValue(8, 4);
	    kidney.setAmount(19);

	    BeanType fire = new BeanType("Fire");
	    fire.setBeanometerValue(3, 1);
	    fire.setBeanometerValue(6, 2);
	    fire.setBeanometerValue(8, 3);
	    fire.setBeanometerValue(9, 4);
	    fire.setAmount(18);

	    BeanType puff = new BeanType("Puff");
	    puff.setBeanometerValue(4, 1);
	    puff.setBeanometerValue(5, 2);
	    puff.setBeanometerValue(6, 3);
	    puff.setBeanometerValue(7, 4);
	    puff.setAmount(16);

	    BeanType broad = new BeanType("Broad");
	    broad.setBeanometerValue(3, 1);
	    broad.setBeanometerValue(5, 2);
	    broad.setBeanometerValue(7, 3);
	    broad.setBeanometerValue(8, 4);
	    broad.setAmount(16);

	    BeanType runner = new BeanType("Runner");
	    runner.setBeanometerValue(3, 1);
	    runner.setBeanometerValue(4, 2);
	    runner.setBeanometerValue(5, 3);
	    runner.setBeanometerValue(6, 4);
	    runner.setAmount(13);

	    BeanType french = new BeanType("French");
	    french.setBeanometerValue(3, 1);
	    french.setBeanometerValue(5, 2);
	    french.setBeanometerValue(7, 3);
	    french.setBeanometerValue(8, 4);
	    french.setAmount(14);

	    this.cards.addAll(this.beanTypeToCards(blue));
	    this.cards.addAll(this.beanTypeToCards(kidney));
	    this.cards.addAll(this.beanTypeToCards(fire));
	    this.cards.addAll(this.beanTypeToCards(puff));
	    this.cards.addAll(this.beanTypeToCards(broad));
	    this.cards.addAll(this.beanTypeToCards(runner));
	    this.cards.addAll(this.beanTypeToCards(french));
	}
	return this.cards;
    }

    @Override
    public List<Phase> getPhases() {

	return null;
    }

    @Override
    public int getNumberOfStartingCards() {
	return 5;
    }

    @Override
    public int getNumberOfStartingFields() {
	return 2;
    }

    @Override
    public int getMaximumFields() {
	return 3;
    }

    @Override
    public int getFieldCost() {
	return 4;
    }

    @Override
    public Player createPlayer(String name) {
	return new Player(name);
    }

    @Override
    public Field createField() {
	return new Field();
    }

    @Override
    public Deck createDeck() {
        return new Deck();
    }

    @Override
    public DiscardPile createDiscardPile() {
        return new DiscardPile();
    }
    
    @Override
    public Table createTable() {
        return new Table();
    }

    public List<List<Card>> getCardsFromPreviousTurn() {
	return cardsFromPreviousTurn;
    }

    public void setCardsFromPreviousTurn(List<List<Card>> cardsFromPreviousTurn) {
	this.cardsFromPreviousTurn = cardsFromPreviousTurn;
    }

    @Override
    public boolean isFinished(Game game) {
	// TODO Auto-generated method stub
	return false;
    }

    public List<Field> getMafiaField() {
	return mafiaField;
    }

    public void setMafiaField(List<Field> mafiaField) {
	this.mafiaField = mafiaField;
    }

    @Override
    public Localization getLocalization() {
	return new EnglishLocalization();
    }

}
