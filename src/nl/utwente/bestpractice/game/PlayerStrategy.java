package nl.utwente.bestpractice.game;

import nl.utwente.bestpractice.phase.Phase;

public interface PlayerStrategy {

    public void takeTurn();

    public void endTurn();

    public void onPhaseStop(Phase phase);

    public void onPhaseStart(Phase phase);

    public void onPhaseInit(Phase phase);

}
