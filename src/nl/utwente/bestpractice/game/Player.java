package nl.utwente.bestpractice.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Deck;
import nl.utwente.bestpractice.model.Field;
import nl.utwente.bestpractice.model.Hand;
import nl.utwente.bestpractice.model.TradingArea;
import nl.utwente.bestpractice.model.Treasury;
import nl.utwente.bestpractice.phase.BeansPlantPhase;
import nl.utwente.bestpractice.phase.Phase;

public class Player extends Observable implements Observer {
    protected String username;
    protected Game game;
    protected Hand hand;
    protected List<Field> fields;
    protected Treasury treasury;
    protected PlayerStrategy strategy;
    protected TradingArea tradingArea;
    
    public enum ObservableChange{
	HAND, TREASURY, FIELDS, TRADING;
    }

    public Player(String username) {
	this.username = username;

	// Setup hand
	this.hand = new Hand();
	this.hand.addObserver(this);

	// Initialize treausry
	this.treasury = new Treasury();
	this.treasury.addObserver(this);

	// Initialize trading area
	this.tradingArea = new TradingArea(this);
	this.tradingArea.addObserver(this);

	// Initialize fields
	this.fields = new ArrayList<Field>();
    }

    public void joinGame(Game game) {
	this.game = game;
    }

    public void onPhaseInit(Phase phase) {
	this.strategy.onPhaseInit(phase);
    }

    /**
     * Called by the game to inform the player that a new Phase will start. This
     * happens also if it is not the turn of this Player. The method is called
     * just after the phase starts
     * 
     * @param phase
     */
    public void onPhaseStart(Phase phase) {
	this.strategy.onPhaseStart(phase);
    }

    /**
     * Called by the game to inform the player that a Phase has stopped. This
     * happens also if it is not the turn of this Player. The method is called
     * just after the phase stops
     * 
     * @param phase
     */
    public void onPhaseStop(Phase phase) {
	this.strategy.onPhaseStop(phase);
    }

    /**
     * Called by the game to inform the player that his/her turn has started
     */
    public void takeTurn() {
	this.strategy.takeTurn();
    }

    /**
     * Called by the game to inform the player that his/her turn has ended
     */
    public void endTurn() {
	this.strategy.endTurn();
    }

    /**
     * Called by the game to inform the player that a trading period has
     * started, where requests can be made in the given area (of a certain
     * Player)
     * 
     * @param tradingArea
     *            The TradingArea this trading round occurs
     */
    /*public void startTrade(TradingArea tradingArea) {
	this.strategy.startTrade(tradingArea);
    }*/

    /**
     * Called by the game to inform the player he/she should plant all cards
     * retrieved in the trading period.
     */
    /*public void stopTrade() {
	this.strategy.stopTrade();
    }*/

    public Treasury getTreasury() {
	return treasury;
    }

    public void setStrategy(PlayerStrategy strategy) {
	this.strategy = strategy;
    }

    public List<Field> getFields() {
	return this.fields;
    }

    public Hand getHand() {
	return this.hand;
    }

    public TradingArea getTradingArea() {
	return this.tradingArea;
    }

    public void drawCard(Deck deck) {
	this.drawCard(deck, 1);
    }

    public void drawCard(Deck deck, int numberOfCards) {
	for (int i = 0; i < numberOfCards; i++) {
	    Card card = deck.drawCard();
	    this.getHand().addCard(card);
	}
	this.notify(ObservableChange.HAND);
    }

    public void addField(Field field) {
	this.fields.add(field);
	field.addObserver(this);
	this.notify(ObservableChange.FIELDS);
    }
    
    public void setGame(Game game) {
	this.game = game;
    }

    /**
     * Plant the given bean on a field
     */
    //public void plantBean(Card card) {
	// TODO Auto-generated method stub

    //}

    /**
     * Plant the given bean on a field, or do noting
     * 
     * @return whether the bean is planted
     */
    //public boolean plantOptionalBean(Card card) {
	//return false;
    //}

    /**
     * ask if the player is interested in this type of cards
     * @param cards
     * @return true if the player is interested indeed
     */
	//public boolean askPlayerInterest(List<Card> cards) {
		// TODO Auto-generated method stub
	//	return false;
	//}

    //public void getTradeRequest(TradingArea tradingArea2) {
	// TODO Auto-generated method stub
	
    //}

    public String getUsername() {
	return username;
    }

    @Override
    public void update(Observable observable, Object data) {
	if(observable.equals(this.hand)){
	    this.notify(ObservableChange.HAND);
	}else if(observable.equals(this.treasury)){
	    this.notify(ObservableChange.TREASURY);
	}else if(observable.equals(this.tradingArea)){
	    this.notify(ObservableChange.TRADING);
	}else if(observable instanceof Field){
	    this.notify(ObservableChange.FIELDS);
	}
    }

    private void notify(ObservableChange change) {
        this.setChanged();
        this.notifyObservers(change);
    }

    public PlayerStrategy getStrategy() {
	return this.strategy;
    }
}
