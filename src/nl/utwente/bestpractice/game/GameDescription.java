package nl.utwente.bestpractice.game;

import java.util.ArrayList;
import java.util.List;

import nl.utwente.bestpractice.localization.Localization;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.Deck;
import nl.utwente.bestpractice.model.DiscardPile;
import nl.utwente.bestpractice.model.Field;
import nl.utwente.bestpractice.model.Table;
import nl.utwente.bestpractice.phase.Phase;

/**
 * Abstract class which contains the description of a game (Abstract Factory).
 * Extensions of the game can implement this class to easily change the game. *
 */
public abstract class GameDescription {
    public abstract int getMinimumPlayers();

    public abstract int getMaximumPlayers();

    public abstract List<Card> getCards();

    public abstract List<Phase> getPhases();

    public abstract int getNumberOfStartingCards();
    
    public abstract int getNumberOfStartingFields();
    
    public abstract int getMaximumFields();
    
    public abstract int getFieldCost();
    
    public abstract Player createPlayer(String name);
    
    public abstract Field createField();
    
    public abstract Deck createDeck();

    public abstract DiscardPile createDiscardPile();

    @Override
    public String toString() {
        return this.getName();
    }
    

    public abstract String getName();

    public abstract boolean isFinished(Game game);

    /**
     * Create the required amount of cards from the given BeanType
     * 
     * @param beanType
     * @return The list of cards
     * @ensure result.size() == beanType.getAmount()
     */
    protected List<Card> beanTypeToCards(BeanType beanType) {
	List<Card> cards = new ArrayList<Card>(beanType.getAmount());
	for (int i = 0; i < beanType.getAmount(); i++) {
	    cards.add(new Card(beanType));
	}
	return cards;
    }

    
    
    /**
     * Get the index of the phase with which the game starts. This only
     * influences the first round of the game, all other rounds start with the
     * first phase
     * 
     * @return The index of the first phase when the game starts
     */
    public int getStartingPhase() {
	// The idea is that this is overwritten in the al cabohne extension
	// (starts with third phase)
	return 0;
    }

    public void onGameStart(Game game){
	
    }

    /**
     * Return a Localization class responsible for the correct localization (translation) of displayed Strings
     * @return
     */
    public abstract Localization getLocalization();

    public abstract Table createTable();
}
