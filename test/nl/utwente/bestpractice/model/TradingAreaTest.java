package nl.utwente.bestpractice.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import nl.utwente.bestpractice.game.Player;
import nl.utwente.bestpractice.game.TradeRequest;

import org.junit.Before;
import org.junit.Test;

public class TradingAreaTest {

	private TradingArea area;
	private BeanType t1;

	@Before
	public void setUp() {
		Player player = new Player("testPlayer");
		this.area = new TradingArea(player);
		this.t1 = new BeanType("t1");
	}

	@Test
	public void testPutFaceUpCard() {
		assertEquals(0, area.getFaceUpCards().size());
		Card c1 = new Card(t1);
		Card c2 = new Card(t1);
		
		area.putFaceUpCard(c1);
		assertTrue(area.getFaceUpCards().contains(c1));
		area.putFaceUpCard(c2);
		assertTrue(area.getFaceUpCards().contains(c1));
		assertTrue(area.getFaceUpCards().contains(c2));
	}

	@Test
	public void testPutTradedCard() {
		assertEquals(0, area.getTradedCards().size());
		Card c1 = new Card(t1);
		Card c2 = new Card(t1);
		
		area.putTradedCard(c1);
		assertTrue(area.getTradedCards().contains(c1));
		area.putTradedCard(c2);
		assertTrue(area.getTradedCards().contains(c1));
		assertTrue(area.getTradedCards().contains(c2));
	}
	
	@Test
	public void testFinishTrading() {
		assertEquals(0, area.getTradedCards().size());
		assertEquals(0, area.getFaceUpCards().size());
		
		// Add cards
		Card c1 = new Card(t1);
		Card c2 = new Card(t1);
		area.putFaceUpCard(c1);
		area.putFaceUpCard(c2);
		assertEquals(0, area.getTradedCards().size());
		assertEquals(2, area.getFaceUpCards().size());
		
		// Add trade request
		TradeRequest r = new TradeRequest(null, null, null);
		area.addTradeRequest(r);
		assertEquals(1, area.getRequests().size());
		
		// Finish trading
		area.finishTrading();
		// Cards should be moved to traded cards pile
		assertEquals(2, area.getTradedCards().size());
		assertEquals(0, area.getFaceUpCards().size());
		// Trade requests should be cleared
		assertEquals(0, area.getRequests().size());
	}

	@Test
	public void testRemoveTradedCard() {
		assertEquals(0, area.getTradedCards().size());
		assertEquals(0, area.getFaceUpCards().size());
		
		// Add cards
		Card c1 = new Card(t1);
		area.putFaceUpCard(c1);
		assertEquals(0, area.getTradedCards().size());
		assertEquals(1, area.getFaceUpCards().size());
		
		// Card should not be removed from face up cards
		area.removeTradedCard(c1);
		assertEquals(0, area.getTradedCards().size());
		assertEquals(1, area.getFaceUpCards().size());
		
		// Finish trading
		area.finishTrading();
		// Card should be removed from traded cards
		area.removeTradedCard(c1);
		assertEquals(0, area.getTradedCards().size());
		assertEquals(0, area.getFaceUpCards().size());
	}

	@Test
	public void testAddTradeRequest() {
		// Null for testing purposes
		TradeRequest r = new TradeRequest(null, null, null);
		
		assertEquals(0, area.getRequests().size());
		area.addTradeRequest(r);
		assertTrue(area.getRequests().contains(r));
	}

	@Test
	public void testRemoveTradeRequest() {
		// Null for testing purposes
		TradeRequest r = new TradeRequest(null, null, null);
		
		assertEquals(0, area.getRequests().size());
		area.addTradeRequest(r);
		assertTrue(area.getRequests().contains(r));
		area.removeTradeRequest(r);
		
	}

	@Test
	public void testClearTradeRequest() {
		// Null for testing purposes
		TradeRequest r = new TradeRequest(null, null, null);
		
		assertEquals(0, area.getRequests().size());
		area.addTradeRequest(r);
		assertTrue(area.getRequests().contains(r));
		area.clearTradeRequest();
		assertEquals(0, area.getRequests().size());
	}

}
