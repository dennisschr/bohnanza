package nl.utwente.bestpractice.model;

import static org.junit.Assert.*;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

public class BeanTypeTest {

	@Test
	public void testGetCoinNumber() {
		BeanType t1 = new BeanType("test");
		t1.setBeanometerValue(1, 1);
		t1.setBeanometerValue(2, 2);
		t1.setBeanometerValue(4, 3);
		
		assertEquals(0, t1.getCoinNumber(0));
		assertEquals(1, t1.getCoinNumber(1));
		assertEquals(2, t1.getCoinNumber(2));
		assertEquals(2, t1.getCoinNumber(3));
		assertEquals(3, t1.getCoinNumber(4));
		assertEquals(3, t1.getCoinNumber(7));
	}
	
	@Test
	public void testGetCoinNoBeanometer(){
		BeanType t1 = new BeanType("test");
		
		assertEquals(0, t1.getCoinNumber(0));
		assertEquals(0, t1.getCoinNumber(1));
		assertEquals(0, t1.getCoinNumber(4));
		assertEquals(0, t1.getCoinNumber(10));
	}

}
