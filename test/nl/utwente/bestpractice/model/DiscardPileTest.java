package nl.utwente.bestpractice.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class DiscardPileTest {

	@Test
	public void testTopCard() {
		DiscardPile pile = new DiscardPile();
		
		BeanType type = new BeanType("test");
		Card c1 = new Card(type);
		Card c2 = new Card(type);
		
		assertNull(pile.getTopCard());
		pile.addCard(c1);
		assertEquals(c1, pile.getTopCard());
		pile.addCard(c2);
		assertEquals(c2, pile.getTopCard());
		pile.empty();
		assertNull(pile.getTopCard());
	}

}
