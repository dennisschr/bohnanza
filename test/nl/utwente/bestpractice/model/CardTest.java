package nl.utwente.bestpractice.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardTest {

	@Test
	public void testBeanType() {
		BeanType t1 = new BeanType("test");
		
		Card c = new Card(t1);
		
		assertEquals(c.getBeanType(), t1);
		
		try{
			Card c2 = new Card(null);
			fail("Card should have a BeanType");
		}catch(IllegalArgumentException e){
			
		}		
	}

}
