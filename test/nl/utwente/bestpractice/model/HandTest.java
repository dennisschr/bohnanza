package nl.utwente.bestpractice.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class HandTest {

	@Test
	public void testHandOrder() {
		Hand h = new Hand();
		
		BeanType t = new BeanType("t");
		
		Card c1 = new Card(t);
		Card c2 = new Card(t);
		Card c3 = new Card(t);
		
		h.addCard(c1);
		h.addCard(c2);
		h.addCard(c3);
		
		assertEquals(c1, h.getCard());
		assertEquals(c2, h.getCard());
		assertEquals(c3, h.getCard());
	}

}
