package nl.utwente.bestpractice.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DeckTest {

	@Test
	public void testIsEmpty() {
		List<Card> cards = new ArrayList<Card>();

		Deck d = new Deck();
		assertTrue(d.isEmpty());
		cards.add(new Card(new BeanType("test")));
		d.addCards(cards);
		assertFalse(d.isEmpty());
		d.drawCard();
		assertTrue(d.isEmpty());
	}

	@Test
	public void testDrawCard() {
		List<Card> cards = new ArrayList<Card>();

		Deck d = new Deck();

		// 1 card
		Card c1 = new Card(new BeanType("test"));
		cards.add(c1);
		d.addCards(cards);
		assertEquals(c1, d.drawCard());

		// more cards
		Card c2 = new Card(new BeanType("test2"));
		cards.add(c1);
		cards.add(c2);
		d.addCards(cards);
		assertNotEquals(d.drawCard(), d.drawCard());
	}

	@Test
	public void testFromDiscardPile() {
		Deck d = new Deck();
		DiscardPile pile = new DiscardPile();
		
		BeanType type = new BeanType("test");
		Card c1 = new Card(type);
		Card c2 = new Card(type);
		
		pile.addCard(c1);
		pile.addCard(c2);
		
		// Fill deck from discard pile
		d.fromDiscardPile(pile);
		
		// Discard pile should be empty
		assertNull(pile.getTopCard());
		
		// Cards should be in deck in some order
		Card c3 = d.drawCard();
		Card c4 = d.drawCard();
		assertNotEquals(c3, c4);
		assertTrue((c3.equals(c1) && c4.equals(c2)) || (c3.equals(c2) && c4.equals(c1)));
	}

}
