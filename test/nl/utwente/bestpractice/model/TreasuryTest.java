package nl.utwente.bestpractice.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TreasuryTest {

	@Before
	public void setUp(){

	}
	
	@Test
	public void testCoinsTreasury() {
		Treasury treasury = new Treasury();
		
		BeanType beanType = new BeanType("bean1");
		Card c1 = new Card(beanType);
		Card c2 = new Card(beanType);
		
		treasury.addCoin(c1);
		assertNotNull(treasury.getListOfCards());
		
		treasury.addCoin(c2);
		assertEquals(2, treasury.getCoins());
		
		treasury.removeCoin();
		assertEquals(1, treasury.getCoins());
		
		Treasury treasury2 = new Treasury();
		Card c = treasury2.removeCoin();
		assertNull(c);
	}
}
