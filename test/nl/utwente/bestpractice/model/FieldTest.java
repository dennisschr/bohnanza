package nl.utwente.bestpractice.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.SortedMap;
import java.util.TreeMap;

import nl.utwente.bestpractice.exception.InvalidBeanTypeException;
import nl.utwente.bestpractice.game.Player;

import org.junit.Before;
import org.junit.Test;

public class FieldTest {
	
	private Field field;
	private BeanType t1;
	private BeanType t2;

	@Before
	public void setup() {
		this.field = new Field();
		
		this.t1 = new BeanType("t1");
		this.t2 = new BeanType("t2");
	}

	@Test
	public void testBeanType() throws InvalidBeanTypeException {
		// No bean in field
		assertNull(field.getBeanType());
		// Add bean
		field.addCard(new Card(t1));
		// Type of field should be the same
		assertEquals(t1, field.getBeanType());
		// Add another bean of the same type
		field.addCard(new Card(t1));
		// Type should still be the same
		assertEquals(t1, field.getBeanType());
	}
		
	@Test
	public void testWrongBeanType() throws InvalidBeanTypeException {
		field.addCard(new Card(t1));
		
		// Add card of wrong BeanType
		try{
			field.addCard(new Card(t2));
			fail("It should not be possible to add a card with another BeanType");
		} catch (InvalidBeanTypeException e) {
		    // correct
		}
	}
	
	@Test
	public void testHarvest() throws InvalidBeanTypeException{
		Player player = new Player("test");
		// Initialize variables (among which is treasury)
		player.joinGame(null);
		DiscardPile pile = new DiscardPile();
		t1.setBeanometerValue(1, 1);
		t1.setBeanometerValue(3, 2);
		
		assertEquals(0, player.getTreasury().getCoins());
		
		// Add one card and harvest
		field.addCard(new Card(t1));
		assertEquals(1, field.getBeanCount());
		field.harvest(player.getTreasury(), pile);
		
		// Card should be in treasury, player should have 1 coin
		assertEquals(0, field.getBeanCount());
		assertEquals(1, player.getTreasury().getCoins());
		assertEquals(0, pile.getCards().size());
		
		// Add three cards and harvest
		field.addCard(new Card(t1));
		field.addCard(new Card(t1));
		field.addCard(new Card(t1));
		assertEquals(3, field.getBeanCount());
		field.harvest(player.getTreasury(), pile);
		
		// Player should have 1+2 coins, there should be 1 card in the pile
		assertEquals(3, player.getTreasury().getCoins());
		assertEquals(0, field.getBeanCount());
		assertEquals(1, pile.getCards().size());
	}

}
