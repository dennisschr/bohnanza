package nl.utwente.bestpractice.game;

import static org.junit.Assert.*;

import java.util.List;

import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;

import org.junit.Test;

public class GameDescriptionTest {

    @Test
    public void testCardsFromBeanType() {
	GameDescription d = new DefaultGameDescription();
	
	// Create BeanType
	BeanType blue = new BeanType("Blue");
	blue.setBeanometerValue(4, 1);
	blue.setBeanometerValue(6, 2);
	blue.setBeanometerValue(8, 3);
	blue.setBeanometerValue(10, 4);
	blue.setAmount(20);
	
	// Create cards
	List<Card> cards = d.beanTypeToCards(blue);
	
	assertEquals(20, cards.size());
	for(Card card : cards){
	    assertEquals(blue, card.getBeanType());
	}
    }

}
