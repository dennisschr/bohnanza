package nl.utwente.bestpractice.game;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import nl.utwente.bestpractice.exception.InvalidCardsException;
import nl.utwente.bestpractice.exception.TradeAlreadyAcceptedException;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;

import org.junit.Test;

public class TradeRequestTest {

	@Test
	public void testAccept() throws InvalidCardsException, TradeAlreadyAcceptedException {
		Player p1 = new Player("1");
		Player p2 = new Player("2");
		
		// Create two cards of different type
		BeanType b1 = new BeanType("b1");
		BeanType b2 = new BeanType("b2");
		Card c1 = new Card(b1);
		Card c2 = new Card(b2);
		
		// Add card 1 to player 1 and card 2 to player 2
		p1.getHand().addCard(c1);
		p2.getHand().addCard(c2);
		
		// Player 2 want to trade 1 b2 card for a b1 card
		Set<Card> offer = new HashSet<Card>();
		offer.add(c2);
		Map<BeanType, Integer> wanted = new HashMap<BeanType, Integer>();
		wanted.put(b1, 1);
		TradeRequest r = new TradeRequest(p2, offer, wanted);
		
		// Player 1 accepts the trade, but does not pass the cards
		Set<Card> cards = new HashSet<Card>();
		try{
			r.accept(p1, cards);
			fail("Trade can only be accepted with correct cards");
		}catch(InvalidCardsException e){
			// Success
		}
		
		// Player 1 accepts the trade with too many cards
		cards.add(new Card(b1));
		cards.add(new Card(b1));
		try{
			r.accept(p1, cards);
			fail("Trade can only be accepted with correct cards");
		}catch(InvalidCardsException e){
			// Success
		}
		
		cards.clear();
		
		// Player 1 accepts the trade with correct cards
		cards.add(c1);
		r.accept(p1, cards);
		
		// Assert that the players have the correct cards
		assertThat(p1.getTradingArea().getTradedCards(), hasItem(c2));
		assertThat(p2.getTradingArea().getTradedCards(), hasItem(c1));
		assertThat(r.isAccepted(), is(true));
		
		// Assert that the cards have been removed from the hands
		assertThat(p1.getHand().getCards(), not(hasItem(c1)));
		assertThat(p2.getHand().getCards(), not(hasItem(c2)));
		
		// Try again
		try{
			r.accept(p1, cards);
			fail("Trade can only be accepted once");
		}catch (TradeAlreadyAcceptedException e){
			// Success
		}
	}	
}
