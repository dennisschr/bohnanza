package nl.utwente.bestpractice.game;

import static org.junit.Assert.*;
import nl.utwente.bestpractice.exception.GameAlreadyStartedException;
import nl.utwente.bestpractice.model.*;

import org.junit.Test;
import org.mockito.Mockito;

public class PlayerTest {

	@Test
	public void testTakeTurn() throws GameAlreadyStartedException {
		Player p1 = new Player("test1");

		PlayerStrategy s = Mockito.mock(PlayerStrategy.class);

		p1.setStrategy(s);

		p1.takeTurn();

		Mockito.verify(s, Mockito.times(1)).takeTurn();
	}
	
	//@Test
	/*public void testStartTrade() throws GameAlreadyStartedException {
		Player p1 = new Player("test1");

		PlayerStrategy s = Mockito.mock(PlayerStrategy.class);

		p1.setStrategy(s);

		TradingArea t = new TradingArea(p1);
		p1.startTrade(t);

		Mockito.verify(s, Mockito.times(1)).startTrade(t);
	}*/
	
	//@Test
	/*public void testStopTrade() throws GameAlreadyStartedException {
		Player p1 = new Player("test1");

		PlayerStrategy s = Mockito.mock(PlayerStrategy.class);

		p1.setStrategy(s);

		p1.stopTrade();

		Mockito.verify(s, Mockito.times(1)).stopTrade();
	}*/

	/*@Test
	public void testPlantBean() {
		BeanType b1 = new BeanType("test1");
		Hand h1 = new Hand();
		Card c1 = new Card(b1);
		Card c2 = new Card(b1);
		h1.addCard(c1);
		h1.addCard(c2);
		Card c = h1.getCard();

		Field f = new Field();
		f.addCard(c1);
		assertEquals(c.getBeanType(), c1.getBeanType());
		assertEquals(c.getBeanType(), f.getBeanType());
		// fail("Not yet implemented");
	}*/

}
