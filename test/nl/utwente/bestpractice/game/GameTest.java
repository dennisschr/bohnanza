package nl.utwente.bestpractice.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import nl.utwente.bestpractice.exception.GameAlreadyStartedException;
import nl.utwente.bestpractice.exception.MaximumFieldsCountReached;
import nl.utwente.bestpractice.exception.NotEnoughCoinsException;
import nl.utwente.bestpractice.exception.InvalidPlayerCountException;
import nl.utwente.bestpractice.model.BeanType;
import nl.utwente.bestpractice.model.Card;
import nl.utwente.bestpractice.model.TradingArea;
import nl.utwente.bestpractice.phase.Phase;

import org.junit.Before;
import org.junit.Test;

public class GameTest {

    private Player p1;
    private Player p2;
    private Player p3;
    private Game g;
    private MockStrategy s1;
    private MockStrategy s2;
    private MockStrategy s3;

    /**
     * This method is called before each test is executed
     */
    @Before
    public void setUp() {
	this.p1 = new Player("test1");
	this.p2 = new Player("test2");
	this.p3 = new Player("test3");
	s1 = new MockStrategy();
	s2 = new MockStrategy();
	s3 = new MockStrategy();

	// Add Mock strategy;
	// We tried doing this using a mocking library, but Mockito did not
	// enable us
	// to assert that of only one of both mocks a method was called
	p1.setStrategy(s1);
	p2.setStrategy(s2);
	p3.setStrategy(s3);

	this.g = new Game(new DefaultGameDescription());
    }

    @Test
    public void testAddPlayer() throws GameAlreadyStartedException,
	    InvalidPlayerCountException {
	// There should be no players in the game
	assertTrue(g.getPlayers().isEmpty());
	// Add the player
	g.addPlayer(p1);
	// Player should be in the game
	assertTrue(g.getPlayers().contains(p1));

	g.addPlayer(p2);
	g.addPlayer(p3);

	assertTrue(g.getPlayers().contains(p1));
	assertTrue(g.getPlayers().contains(p2));
	assertTrue(g.getPlayers().contains(p3));

	// Start game
	g.start();

	// Try adding another player
	try {
	    g.addPlayer(new Player("test4"));
	    fail("You should not be able to add player after the game has started");
	} catch (GameAlreadyStartedException e) {
	    // Success
	}
    }

    @Test
    public void testStart() throws GameAlreadyStartedException {
	// At first there are no players in the game
	try {
	    g.start();
	    fail("Game should have 3 - 7 players");
	} catch (InvalidPlayerCountException e) {
	    // Success
	}

	// Add 2 players
	g.addPlayer(p1);
	g.addPlayer(p2);

	// Test again
	try {
	    g.start();
	    fail("Game should have 3 - 7 players");
	} catch (InvalidPlayerCountException e) {
	    // Success
	}

	// Add the 3rd player
	g.addPlayer(p3);

	// Game should start now
	try {
	    g.start();
	} catch (InvalidPlayerCountException e) {
	    fail("Game should be able to start with 3 players");
	}
    }

    @Test
    public void testGiveTurn() throws GameAlreadyStartedException {
	g.addPlayer(p1);
	g.addPlayer(p2);

	g.nextPlayer();
	// One should have had a turn
	assertEquals(1, s1.takeTurnCount + s2.takeTurnCount);
	g.nextPlayer();
	// Both should have had a turn
	assertEquals(1, s1.takeTurnCount);
	assertEquals(1, s2.takeTurnCount);
    }

    /*@Test
    public void testStartTrade() throws GameAlreadyStartedException {
	g.addPlayer(p1);
	g.addPlayer(p2);

	// Start trade
	g.startTrade(new TradingArea(p1));

	// Both should have had a turn
	assertEquals(1, s1.startTradeCount);
	assertEquals(1, s2.startTradeCount);
    }

    @Test
    public void testStopTrade() throws GameAlreadyStartedException {
	g.addPlayer(p1);
	g.addPlayer(p2);

	// Start trade
	TradingArea t = spy(new TradingArea(p1));
	g.startTrade(t);
	g.stopTrade();

	// finishTrading should have been called
	verify(t, times(1)).finishTrading();

	// Both should have had a turn
	assertEquals(1, s1.startTradeCount);
	assertEquals(1, s2.startTradeCount);
    }*/

    @Test
    public void testBuyField() throws MaximumFieldsCountReached,
	    NotEnoughCoinsException, GameAlreadyStartedException,
	    InvalidPlayerCountException {
	// TODO make amount of coins needed not hard coded

	// Add the player to the game
	g.addPlayer(p1);
	g.addPlayer(p2);
	g.addPlayer(p3);
	g.start();

	// Assert the player has 2 fields
	assertEquals(g.getDescription().getNumberOfStartingFields(), p1
		.getFields().size());

	// Try to buy a card as player without coins
	try {
	    g.buyField(p1);
	    fail("Player should have enough coins to buy a field");
	} catch (NotEnoughCoinsException e) {
	    // Pass
	}

	// Add needed coins minus 1
	BeanType type = new BeanType("bean");
	for (int i = 1; i < g.getDescription().getFieldCost(); i++) {
	    p1.getTreasury().addCoin(new Card(type));
	}

	// Try to buy a card as player without enough coins
	try {
	    g.buyField(p1);
	    fail("Player should have enough coins to buy a field");
	} catch (NotEnoughCoinsException e) {
	    // Pass
	}

	// Add another coin
	p1.getTreasury().addCoin(new Card(type));
	// Should be succesfull
	g.buyField(p1);

	// Assert the player has 3 fields
	assertEquals(g.getDescription().getNumberOfStartingFields() + 1, p1
		.getFields().size());
	// Assert the player has 0 coins
	assertEquals(0, p1.getTreasury().getCoins());

	// Buy fields untill the maximum is reached
	int k = p1.getFields().size();
	while (k < g.getDescription().getMaximumFields()) {
	    // Try buying another field
	    // Add needed coins
	    for (int i = 0; i < g.getDescription().getFieldCost(); i++) {
		p1.getTreasury().addCoin(new Card(type));
	    }
	    g.buyField(p1);
	    k = p1.getFields().size();
	}

	// Try buying another one
	for (int i = 0; i < g.getDescription().getFieldCost(); i++) {
	    p1.getTreasury().addCoin(new Card(type));
	}
	try {
	    g.buyField(p1);
	    fail("Player can not have more than the maximum number of fields");
	} catch (MaximumFieldsCountReached e) {
	    // Pass
	}

	// Assert the player has 3 fields
	assertEquals(g.getDescription().getMaximumFields(), p1.getFields().size());

	// Assert the player has 3 coins
	assertEquals(g.getDescription().getFieldCost(), p1.getTreasury()
		.getCoins());
    }

    class MockStrategy implements PlayerStrategy {

	public int startTradeCount;
	public int takeTurnCount;
	public int stopTradeCount;

	public void takeTurn() {
	    this.takeTurnCount++;
	}

	public void startTrade(TradingArea area) {
	    this.startTradeCount++;
	}

	public void stopTrade() {
	    this.stopTradeCount++;
	}

	public void endTurn() {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onPhaseStop(Phase phase) {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onPhaseStart(Phase phase) {
	    // TODO Auto-generated method stub
	    
	}

	@Override
	public void onPhaseInit(Phase phase) {
	    // TODO Auto-generated method stub
	    
	}

    }

}
