# Bohnanza

## Compiling

If you import the project in Eclipse, the dependencies should automatically be downloaded (using maven plugin of Eclipse), and compiled.

It could be the case that you have to add the resources directory as source folder by right clicking and selecting "Build path" > "Use as source folder". 

To compile the game to a stand-alone jar, execute the following command in the root directory of the project:

	mvn clean compile assembly:single
	
In Eclipse, you can also right click the pom.xml, select "Run as" > "Maven build". 
In the "Goals" input field of the screen insert "clean compile assembly:single" and press "Run".

This will build a single jar file including all dependencies in the target directory using Maven.

## Starting

To start the game, run the class BohnanzaApplication in the nl.utwente.bestpractice.gui.javafx package.

## Testing

The tests can be executed by right clicking on the test directory in Eclipse and select "Run as" > "Junit test".

Testing can also be performed using maven, using the command:

	mvn test
	
or right clicking the pom.xml and select "Run as" > "Maven test".